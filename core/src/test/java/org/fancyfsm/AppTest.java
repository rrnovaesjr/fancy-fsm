package org.fancyfsm;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    void shouldAnswerWithTrue() {
        Assertions.assertThat(1).isEqualTo(1);
    }
}
