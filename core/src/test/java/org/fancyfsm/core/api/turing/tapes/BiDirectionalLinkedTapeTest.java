package org.fancyfsm.core.api.turing.tapes;

import org.assertj.core.api.Assertions;
import org.fancyfsm.core.api.deterministic.turing.transitions.DeterministicTuringTransitionResult;
import org.fancyfsm.core.api.turing.tapes.commands.HeadPositionSwap;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

class BiDirectionalLinkedTapeTest {

  @Test
  void shouldWriteToLeftOfTheIndex0() {
    BiDirectionalLinkedTape<String> infiniteTape = new BiDirectionalLinkedTape<>();

    infiniteTape.shiftHead(HeadPositionSwap.LEFT);
    infiniteTape.writeToHead("Alia");

    Assertions.assertThat(infiniteTape.readFromHead()).isEqualTo("Alia");
  }

  @Test
  void shouldExecuteAllSetsOfCommands() {
    BiDirectionalLinkedTape<String> infiniteTape = new BiDirectionalLinkedTape<>();

    infiniteTape.writeToHead("Mega Man X");
    infiniteTape.shiftHead(HeadPositionSwap.LEFT);
    infiniteTape.writeToHead("Zero");
    infiniteTape.shiftHead(HeadPositionSwap.LEFT);
    infiniteTape.shiftHead(HeadPositionSwap.LEFT);
    infiniteTape.writeToHead("Axl");

    Assertions.assertThat(infiniteTape.readFromHead()).isEqualTo("Axl");
    infiniteTape.shiftHead(HeadPositionSwap.RIGHT);
    Assertions.assertThat(infiniteTape.readFromHead()).isNull();
    infiniteTape.shiftHead(HeadPositionSwap.RIGHT);
    Assertions.assertThat(infiniteTape.readFromHead()).isEqualTo("Zero");
    infiniteTape.shiftHead(HeadPositionSwap.RIGHT);
    Assertions.assertThat(infiniteTape.readFromHead()).isEqualTo("Mega Man X");
  }

  @Test
  void shouldWriteInAnyPosition() {
    BiDirectionalLinkedTape<Object> infiniteTape = new BiDirectionalLinkedTape<>();

    infiniteTape.shiftHead(HeadPositionSwap.RIGHT);
    infiniteTape.shiftHead(HeadPositionSwap.RIGHT);

    infiniteTape.writeToHead("100");
    Assertions.assertThat(infiniteTape.readFromHead()).isEqualTo("100");

    infiniteTape.shiftHead(HeadPositionSwap.RIGHT);
    infiniteTape.shiftHead(HeadPositionSwap.RIGHT);
    infiniteTape.shiftHead(HeadPositionSwap.RIGHT);
    infiniteTape.shiftHead(HeadPositionSwap.RIGHT);
    infiniteTape.shiftHead(HeadPositionSwap.RIGHT);

    infiniteTape.writeToHead("1012");
    Assertions.assertThat(infiniteTape.readFromHead()).isEqualTo("1012");

    infiniteTape.shiftHead(HeadPositionSwap.LEFT);
    infiniteTape.shiftHead(HeadPositionSwap.LEFT);
    infiniteTape.shiftHead(HeadPositionSwap.LEFT);
    infiniteTape.shiftHead(HeadPositionSwap.LEFT);
    infiniteTape.shiftHead(HeadPositionSwap.LEFT);

    Assertions.assertThat(infiniteTape.readFromHead()).isEqualTo("100");

    infiniteTape.shiftHead(HeadPositionSwap.LEFT);
    infiniteTape.shiftHead(HeadPositionSwap.LEFT);
    infiniteTape.shiftHead(HeadPositionSwap.LEFT);
    infiniteTape.shiftHead(HeadPositionSwap.LEFT);
    infiniteTape.shiftHead(HeadPositionSwap.LEFT);
    infiniteTape.shiftHead(HeadPositionSwap.LEFT);

    infiniteTape.writeToHead("-100");
    Assertions.assertThat(infiniteTape.readFromHead()).isEqualTo("-100");

    infiniteTape.shiftHead(HeadPositionSwap.LEFT);
    infiniteTape.shiftHead(HeadPositionSwap.LEFT);
    infiniteTape.shiftHead(HeadPositionSwap.LEFT);
    infiniteTape.shiftHead(HeadPositionSwap.LEFT);
    infiniteTape.shiftHead(HeadPositionSwap.LEFT);

    infiniteTape.writeToHead("-1012");
    Assertions.assertThat(infiniteTape.readFromHead()).isEqualTo("-1012");

    infiniteTape.shiftHead(HeadPositionSwap.RIGHT);
    infiniteTape.shiftHead(HeadPositionSwap.RIGHT);
    infiniteTape.shiftHead(HeadPositionSwap.RIGHT);
    infiniteTape.shiftHead(HeadPositionSwap.RIGHT);
    infiniteTape.shiftHead(HeadPositionSwap.RIGHT);

    Assertions.assertThat(infiniteTape.readFromHead()).isEqualTo("-100");

    infiniteTape.shiftHead(HeadPositionSwap.RIGHT);
    infiniteTape.shiftHead(HeadPositionSwap.RIGHT);
    infiniteTape.shiftHead(HeadPositionSwap.RIGHT);
    infiniteTape.shiftHead(HeadPositionSwap.RIGHT);

    infiniteTape.writeToHead("0");
    Assertions.assertThat(infiniteTape.readFromHead()).isEqualTo("0");

    infiniteTape.shiftHead(HeadPositionSwap.LEFT);

    infiniteTape.writeToHead("-1");
    Assertions.assertThat(infiniteTape.readFromHead()).isEqualTo("-1");
  }

  @Test
  void shouldThrowIllegalArgumentExceptionWhenCommandIsNull() {
    BiDirectionalLinkedTape<Object> infiniteTape = new BiDirectionalLinkedTape<>();

    Assertions.assertThatCode(() -> infiniteTape.shiftHead(null))
        .isExactlyInstanceOf(IllegalArgumentException.class)
        .hasMessage("Command cannot be null");
  }

  @Test
  void shouldWriteAtRandomSelectedPosition() {
    BiDirectionalLinkedTape<String> biDirectionalLinkedTape = new BiDirectionalLinkedTape<>();

    for (int i = 0; i < 1000; i++) biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);

    biDirectionalLinkedTape.writeToHead("-1000");
    Assertions.assertThat(biDirectionalLinkedTape.readFromHead()).isEqualTo("-1000");

    for (int i = 0; i < 2000; i++) biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);

    biDirectionalLinkedTape.writeToHead("1000");
    Assertions.assertThat(biDirectionalLinkedTape.readFromHead()).isEqualTo("1000");
  }

  private static Stream<Arguments> provideCommandsAndExpectedSize() {
    return Stream.of(
        Arguments.of(
            List.of(new DeterministicTuringTransitionResult<>(null, 1, HeadPositionSwap.LEFT)), 1),
        Arguments.of(
            List.of(new DeterministicTuringTransitionResult<>(null, null, HeadPositionSwap.LEFT)),
            0),
        Arguments.of(
            List.of(
                new DeterministicTuringTransitionResult<>(null, null, HeadPositionSwap.RIGHT),
                new DeterministicTuringTransitionResult<>(null, null, HeadPositionSwap.RIGHT),
                new DeterministicTuringTransitionResult<>(null, 1, HeadPositionSwap.LEFT),
                new DeterministicTuringTransitionResult<>(null, 0, HeadPositionSwap.RIGHT),
                new DeterministicTuringTransitionResult<>(null, null, HeadPositionSwap.LEFT)),
            1),
        Arguments.of(
            List.of(
                new DeterministicTuringTransitionResult<>(null, 1, HeadPositionSwap.RIGHT),
                new DeterministicTuringTransitionResult<>(null, null, HeadPositionSwap.RIGHT),
                new DeterministicTuringTransitionResult<>(null, 1, HeadPositionSwap.RIGHT),
                new DeterministicTuringTransitionResult<>(null, null, HeadPositionSwap.RIGHT),
                new DeterministicTuringTransitionResult<>(null, 0, HeadPositionSwap.LEFT)),
            3));
  }

  @ParameterizedTest
  @MethodSource("provideCommandsAndExpectedSize")
  void shouldKnowExactSizeOfTape(
      List<DeterministicTuringTransitionResult<Object, Integer, HeadPositionSwap>> commands,
      final int expectedSize) {

    var biDirectionalLinkedTape = new BiDirectionalLinkedTape<>();

    commands.forEach(
        command -> {
          biDirectionalLinkedTape.writeToHead(command.symbol());
          biDirectionalLinkedTape.shiftHead(command.direction());
        });

    Assertions.assertThat(biDirectionalLinkedTape.size()).isEqualTo(expectedSize);
  }

  private static Stream<Arguments> provideIterationParameters() {
    return Stream.of(
            Arguments.of(
                    "One Piece",
                    Arrays.asList(
                            'P', 'i', 'e', 'c', 'e', null, 'e', 'c', 'e', 'i', 'P', ' ', 'e', 'n', 'O', null)));
  }

  @ParameterizedTest
  @MethodSource("provideIterationParameters")
  void shouldAllowToIterateTapeHead(String expectedWord, List<Character> alphabet) {
    BiDirectionalLinkedTape<Character> rightInfiniteTape = new BiDirectionalLinkedTape<>();
    var position = HeadPositionSwap.RIGHT;
    for (var symbol : alphabet) {
      rightInfiniteTape.writeToHead(symbol);
      if (symbol == null) position = HeadPositionSwap.LEFT;
      rightInfiniteTape.shiftHead(position);
    }

    StringBuilder actualWord = new StringBuilder();
    for (var symbol : rightInfiniteTape) {
      actualWord.append(symbol);
    }
    Assertions.assertThat(actualWord).hasToString(expectedWord);

    actualWord = new StringBuilder();
    var iterator = rightInfiniteTape.iterator();
    while (iterator.hasNext()) {
      actualWord.append(iterator.next());
    }
    Assertions.assertThat(actualWord).hasToString(expectedWord);

    actualWord = new StringBuilder();
    rightInfiniteTape.stream().forEach(actualWord::append);
    Assertions.assertThat(actualWord).hasToString(expectedWord);
  }
}
