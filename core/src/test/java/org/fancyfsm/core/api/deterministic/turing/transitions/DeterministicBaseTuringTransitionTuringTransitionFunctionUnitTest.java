package org.fancyfsm.core.api.deterministic.turing.transitions;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Set;
import java.util.stream.Stream;

class DeterministicBaseTuringTransitionTuringTransitionFunctionUnitTest {

  @Test
  void shouldThrowNullPointerExceptionWhenCreatingTransitionFunctionWithNullTransitionsSet() {
    Assertions.assertThatCode(() -> new DeterministicTuringTransitionFunction<>(null))
        .isExactlyInstanceOf(NullPointerException.class)
        .hasMessage("Transitions set cannot be null");
  }

  @Test
  void shouldThrowIllegalStateExceptionWhenDuplicateStateActionPairExists() {
    var transitionA = new TestTransitionDomainBaseTuringTransition(null, null, "abc", 'a', 0);
    var transitionB = new TestTransitionDomainBaseTuringTransition(null, null, "def", 'b', 1);
    Assertions.assertThatCode(
            () -> new DeterministicTuringTransitionFunction<>(Set.of(transitionA, transitionB)))
        .isExactlyInstanceOf(IllegalStateException.class)
        .hasMessageMatching(
            "^Duplicate key StateSymbolPair\\[state=null, symbol=null] \\(attempted merging values .* and .*\\)$");
  }

  private static Stream<Arguments> provideHasTransitionValues() {
    return Stream.of(
        Arguments.of("initial", '0', true),
        Arguments.of("initial", '1', true),
        Arguments.of("initial", '*', true),
        Arguments.of("final", '*', false));
  }

  @ParameterizedTest
  @MethodSource("provideHasTransitionValues")
  void shouldHaveTransition(String sourceState, Character symbol, boolean expectedHasTransition) {
    var transitionFunction = defaultFunction();

    boolean actualHasTransition = transitionFunction.hasTransition(sourceState, symbol);

    Assertions.assertThat(actualHasTransition).isEqualTo(expectedHasTransition);
  }

  private static Stream<Arguments> provideShouldComputeValues() {
    return Stream.of(
        Arguments.of("initial", '0', new DeterministicTuringTransitionResult<>("initial", '1', 1)),
        Arguments.of("initial", '1', new DeterministicTuringTransitionResult<>("initial", '0', 1)),
        Arguments.of("initial", '*', new DeterministicTuringTransitionResult<>("final", '*', 0)));
  }

  @ParameterizedTest
  @MethodSource("provideShouldComputeValues")
  void shouldCompute(
      String sourceState,
      Character symbol,
      DeterministicTuringTransitionResult<String, Character, Integer> expectedTransitionResult) {
    var transitionFunction = defaultFunction();

    var actualTransitionResult = transitionFunction.compute(sourceState, symbol);

    Assertions.assertThat(actualTransitionResult).hasValue(expectedTransitionResult);
  }

  @Test
  void shouldNotCompute() {
    var transitionFunction = defaultFunction();

    var actualTransitionResult = transitionFunction.compute("final", '0');

    Assertions.assertThat(actualTransitionResult).isEmpty();
  }

  private DeterministicTuringTransitionFunction<String, Character, Integer> defaultFunction() {
    return new DeterministicTuringTransitionFunction<>(
        Set.of(
            new TestTransitionDomainBaseTuringTransition("initial", '0', "initial", '1', 1),
            new TestTransitionDomainBaseTuringTransition("initial", '1', "initial", '0', 1),
            new TestTransitionDomainBaseTuringTransition("initial", '*', "final", '*', 0)));
  }

  @Accessors(fluent = true)
  @Getter
  @AllArgsConstructor
  @EqualsAndHashCode
  @ToString
  private static class TestTransitionDomainBaseTuringTransition
      implements DeterministicTuringTransition<String, Character, Integer> {
    private final String sourceState;
    private final Character symbol;
    private final String nextState;
    private final Character newSymbol;
    private final Integer direction;
  }
}
