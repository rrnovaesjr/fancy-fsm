package org.fancyfsm.core.api.turing.tapes;

import org.assertj.core.api.Assertions;
import org.fancyfsm.core.api.turing.tapes.commands.HeadPositionSwap;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

class ArrayTapeTest {

  @Test
  void shouldNotShiftHeaderToLeftWhenAt0Index() {
    ArrayTape<Object> rightInfiniteTape = new ArrayTape<>();

    Assertions.assertThatCode(() -> rightInfiniteTape.shiftHead(HeadPositionSwap.LEFT))
        .isExactlyInstanceOf(IndexOutOfBoundsException.class)
        .hasMessage("Cannot move to the left of the 0 index");
  }

  @Test
  void shouldExecuteAllSetsOfCommands() {
    ArrayTape<String> rightInfiniteTape = new ArrayTape<>();

    rightInfiniteTape.writeToHead("Mega Man X");
    rightInfiniteTape.shiftHead(HeadPositionSwap.RIGHT);
    rightInfiniteTape.writeToHead("Zero");
    rightInfiniteTape.shiftHead(HeadPositionSwap.RIGHT);
    rightInfiniteTape.shiftHead(HeadPositionSwap.RIGHT);
    rightInfiniteTape.writeToHead("Axl");

    Assertions.assertThat(rightInfiniteTape.readFromHead()).isEqualTo("Axl");
    rightInfiniteTape.shiftHead(HeadPositionSwap.LEFT);
    Assertions.assertThat(rightInfiniteTape.readFromHead()).isNull();
    rightInfiniteTape.shiftHead(HeadPositionSwap.LEFT);
    Assertions.assertThat(rightInfiniteTape.readFromHead()).isEqualTo("Zero");
    rightInfiniteTape.shiftHead(HeadPositionSwap.LEFT);
    Assertions.assertThat(rightInfiniteTape.readFromHead()).isEqualTo("Mega Man X");
  }

  @Test
  void shouldWriteEvenBeyondInitialCapacity() {
    ArrayTape<Object> rightInfiniteTape = new ArrayTape<>(1);

    rightInfiniteTape.shiftHead(HeadPositionSwap.RIGHT);
    rightInfiniteTape.shiftHead(HeadPositionSwap.RIGHT);

    rightInfiniteTape.writeToHead("100");
    Assertions.assertThat(rightInfiniteTape.readFromHead()).isEqualTo("100");

    rightInfiniteTape.shiftHead(HeadPositionSwap.RIGHT);
    rightInfiniteTape.shiftHead(HeadPositionSwap.RIGHT);
    rightInfiniteTape.shiftHead(HeadPositionSwap.RIGHT);
    rightInfiniteTape.shiftHead(HeadPositionSwap.RIGHT);
    rightInfiniteTape.shiftHead(HeadPositionSwap.RIGHT);

    rightInfiniteTape.writeToHead("1012");
    Assertions.assertThat(rightInfiniteTape.readFromHead()).isEqualTo("1012");

    rightInfiniteTape.shiftHead(HeadPositionSwap.LEFT);
    rightInfiniteTape.shiftHead(HeadPositionSwap.LEFT);
    rightInfiniteTape.shiftHead(HeadPositionSwap.LEFT);
    rightInfiniteTape.shiftHead(HeadPositionSwap.LEFT);
    rightInfiniteTape.shiftHead(HeadPositionSwap.LEFT);

    Assertions.assertThat(rightInfiniteTape.readFromHead()).isEqualTo("100");
  }

  @Test
  void shouldThrowIllegalArgumentExceptionWhenCommandIsNull() {
    ArrayTape<Object> arrayTape = new ArrayTape<>();

    Assertions.assertThatCode(() -> arrayTape.shiftHead(null))
        .isExactlyInstanceOf(IllegalArgumentException.class)
        .hasMessage("Command cannot be null");
  }

  private static Stream<Arguments> provideIterationParameters() {
    return Stream.of(
        Arguments.of(
            "One Piece",
            Arrays.asList(
                null, 'O', 'n', 'e', null, ' ', null, null, 'P', null, 'i', null, 'e', 'c', 'e',
                null)));
  }

  @ParameterizedTest
  @MethodSource("provideIterationParameters")
  void shouldAllowToIterateTapeHead(String expectedWord, List<Character> alphabet) {
    ArrayTape<Character> rightInfiniteTape = new ArrayTape<>();
    for (var symbol : alphabet) {
      rightInfiniteTape.writeToHead(symbol);
      rightInfiniteTape.shiftHead(HeadPositionSwap.RIGHT);
    }

    StringBuilder actualWord = new StringBuilder();
    for (var symbol : rightInfiniteTape) {
      actualWord.append(symbol);
    }
    Assertions.assertThat(actualWord).hasToString(expectedWord);

    actualWord = new StringBuilder();
    var iterator = rightInfiniteTape.iterator();
    while (iterator.hasNext()) {
      actualWord.append(iterator.next());
    }
    Assertions.assertThat(actualWord).hasToString(expectedWord);

    actualWord = new StringBuilder();
    rightInfiniteTape.stream().forEach(actualWord::append);
    Assertions.assertThat(actualWord).hasToString(expectedWord);
  }
}
