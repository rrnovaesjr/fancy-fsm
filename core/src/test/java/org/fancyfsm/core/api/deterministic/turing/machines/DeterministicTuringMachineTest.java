package org.fancyfsm.core.api.deterministic.turing.machines;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.assertj.core.api.Assertions;
import org.fancyfsm.core.api.deterministic.states.DefaultDeterministicStateContext;
import org.fancyfsm.core.api.deterministic.states.DeterministicStateContext;
import org.fancyfsm.core.api.deterministic.states.memory.PlainObjectDeterministicStateMemory;
import org.fancyfsm.core.api.turing.tapes.Tape;
import org.fancyfsm.core.api.deterministic.turing.transitions.DeterministicTuringTransition;
import org.fancyfsm.core.api.deterministic.turing.transitions.DeterministicTuringTransitionFunction;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

class DeterministicTuringMachineTest {

  @Test
  void shouldInvertTextCase() {
    SwitcherTape tapeHeader = tapeHeader("KarenIsMyLove");
    var turingMachine = machine(tapeHeader);

    turingMachine.compute();

    Assertions.assertThat(tapeHeader.getWord()).hasToString("kARENiSmYlOVE");
    Assertions.assertThat(turingMachine.isAccepted()).isTrue();
  }

  private DeterministicTuringMachine<SwitcherState, Character, Integer> machine(
      SwitcherTape tapeHeader) {
    return new DeterministicTuringMachine<>(
        createStateContext(), tapeHeader, Set.of(SwitcherState.FINISHED), createFunction());
  }

  private SwitcherTape tapeHeader(String input) {
    return new SwitcherTape(new StringBuilder(input));
  }

  private DeterministicStateContext<SwitcherState> createStateContext() {
    return new DefaultDeterministicStateContext<>(
        SwitcherState.SWITCHING, new PlainObjectDeterministicStateMemory<>());
  }

  private DeterministicTuringTransitionFunction<SwitcherState, Character, Integer>
      createFunction() {
    var alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ\0".toCharArray();

    Set<DeterministicTuringTransition<SwitcherState, Character, Integer>> transitions =
        new HashSet<>();

    for (var symbol : alphabet) {
      transitions.add(new SwitcherTransition(SwitcherState.SWITCHING, symbol));
    }
    return new DeterministicTuringTransitionFunction<>(transitions);
  }

  private enum SwitcherState {
    SWITCHING,
    FINISHED
  }

  @RequiredArgsConstructor
  private static class SwitcherTape implements Tape<Character, Integer> {
    private int index = 0;
    @Getter private final StringBuilder word;

    @Override
    public Character readFromHead() {
      if (index >= word.length()) return '\0';
      return word.charAt(index);
    }

    @Override
    public void writeToHead(Character symbol) {
      if (symbol == null) return;
      word.replace(index, index + 1, "" + symbol);
    }

    @Override
    public void shiftHead(Integer command) {
      index += command;
    }
  }

  @Accessors(fluent = true)
  @Getter
  @RequiredArgsConstructor
  private static class SwitcherTransition
      implements DeterministicTuringTransition<SwitcherState, Character, Integer> {
    private final SwitcherState sourceState;
    private final Character symbol;

    @Override
    public Integer direction() {
      return 1;
    }

    @Override
    public Character newSymbol() {
      if (symbol == '\0' || sourceState == SwitcherState.FINISHED) return null;
      if (Character.isUpperCase(symbol)) return Character.toLowerCase(symbol);
      return Character.toUpperCase(symbol);
    }

    @Override
    public SwitcherState nextState() {
      if (symbol == '\0' || sourceState == SwitcherState.FINISHED) return SwitcherState.FINISHED;
      return SwitcherState.SWITCHING;
    }
  }
}
