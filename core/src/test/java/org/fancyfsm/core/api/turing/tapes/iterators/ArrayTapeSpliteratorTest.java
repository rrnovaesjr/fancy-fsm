package org.fancyfsm.core.api.turing.tapes.iterators;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Spliterator;
import java.util.stream.Stream;

class ArrayTapeSpliteratorTest {

  @Test
  void shouldTryAdvanceOverNonNullSymbols() {
    List<Integer> expectedList = List.of(1, 0, 0, 1, 1, 0, 1, 0);
    Integer[] alphabet = {
      null, 1, 0, 0, 1, null, null, null, null, null, 1, 0, 1, 0, null, null, null, null, null
    };
    ArrayTapeSpliterator<Integer> arrayTapeSpliterator = new ArrayTapeSpliterator<>(alphabet);

    List<Integer> actualList = new ArrayList<>();

    Assertions.assertThat(arrayTapeSpliterator.tryAdvance(actualList::add)).isTrue();
    Assertions.assertThat(arrayTapeSpliterator.tryAdvance(actualList::add)).isTrue();
    Assertions.assertThat(arrayTapeSpliterator.tryAdvance(actualList::add)).isTrue();
    Assertions.assertThat(arrayTapeSpliterator.tryAdvance(actualList::add)).isTrue();
    Assertions.assertThat(arrayTapeSpliterator.tryAdvance(actualList::add)).isTrue();
    Assertions.assertThat(arrayTapeSpliterator.tryAdvance(actualList::add)).isTrue();
    Assertions.assertThat(arrayTapeSpliterator.tryAdvance(actualList::add)).isTrue();
    Assertions.assertThat(arrayTapeSpliterator.tryAdvance(actualList::add)).isTrue();
    Assertions.assertThat(arrayTapeSpliterator.tryAdvance(actualList::add)).isFalse();
    Assertions.assertThat(actualList).isEqualTo(expectedList);
  }

  private static Stream<Arguments> provideExpectedListAndAlphabet() {

    return Stream.of(
        Arguments.of(
            List.of(1, 0, 0, 1, 1, 0, 1, 0),
            new Integer[] {
              null, 1, 0, 0, 1, null, null, null, null, null, 1, 0, 1, 0, null, null, null, null,
              null
            }),
        Arguments.of(Collections.emptyList(), new Integer[] {null, null, null, null, null}),
        Arguments.of(List.of(1), new Integer[] {1}));
  }

  @ParameterizedTest
  @MethodSource("provideExpectedListAndAlphabet")
  void shouldTryAdvanceOverNonNullSymbolsWithForEachRemaining(
      List<Integer> expectedList, Integer[] alphabet) {
    ArrayTapeSpliterator<Integer> arrayTapeSpliterator = new ArrayTapeSpliterator<>(alphabet);
    List<Integer> actualList = new ArrayList<>();

    arrayTapeSpliterator.forEachRemaining(actualList::add);

    Assertions.assertThat(actualList).isEqualTo(expectedList);
  }

  @ParameterizedTest
  @MethodSource("provideExpectedListAndAlphabet")
  void shouldGetExactSizeOfSpliterator(List<Integer> expectedList, Integer[] alphabet) {
    final long expectedSize = expectedList.size();
    ArrayTapeSpliterator<Integer> arrayTapeSpliterator = new ArrayTapeSpliterator<>(alphabet);

    final long actualSize = arrayTapeSpliterator.getExactSizeIfKnown();

    Assertions.assertThat(actualSize).isEqualTo(expectedSize);
  }

  @Test
  void shouldReduceSizeOfSpliteratorAtEachIteration() {
    Integer[] alphabet = {
      null, 1, 0, null, 1, 0, null, null, 1, 0, null, null, null, 1, null, 0, null
    };
    ArrayTapeSpliterator<Integer> arrayTapeSpliterator = new ArrayTapeSpliterator<>(alphabet);

    Assertions.assertThat(arrayTapeSpliterator.getExactSizeIfKnown()).isEqualTo(8);
    arrayTapeSpliterator.tryAdvance(System.out::println);
    Assertions.assertThat(arrayTapeSpliterator.getExactSizeIfKnown()).isEqualTo(7);
    arrayTapeSpliterator.tryAdvance(System.out::println);
    Assertions.assertThat(arrayTapeSpliterator.getExactSizeIfKnown()).isEqualTo(6);
    arrayTapeSpliterator.tryAdvance(System.out::println);
    Assertions.assertThat(arrayTapeSpliterator.getExactSizeIfKnown()).isEqualTo(5);
    arrayTapeSpliterator.tryAdvance(System.out::println);
    Assertions.assertThat(arrayTapeSpliterator.getExactSizeIfKnown()).isEqualTo(4);
    arrayTapeSpliterator.tryAdvance(System.out::println);
    Assertions.assertThat(arrayTapeSpliterator.getExactSizeIfKnown()).isEqualTo(3);
    arrayTapeSpliterator.tryAdvance(System.out::println);
    Assertions.assertThat(arrayTapeSpliterator.getExactSizeIfKnown()).isEqualTo(2);
    arrayTapeSpliterator.tryAdvance(System.out::println);
    Assertions.assertThat(arrayTapeSpliterator.getExactSizeIfKnown()).isEqualTo(1);
    arrayTapeSpliterator.tryAdvance(System.out::println);
    Assertions.assertThat(arrayTapeSpliterator.getExactSizeIfKnown()).isEqualTo(0);
  }

  private static Stream<Arguments> provideExpectedListsAndSplitListsAndAlphabet() {
    return Stream.of(
        Arguments.of(
            List.of(1, 0, 0, 1),
            List.of(1, 0, 1, 0),
            new Integer[] {
              null, 1, 0, 0, 1, null, null, null, null, null, 1, 0, 1, 0, null, null, null, null,
              null
            }),
        Arguments.of(
            Collections.emptyList(),
            Collections.emptyList(),
            new Integer[] {null, null, null, null, null}),
        Arguments.of(Collections.emptyList(), List.of(1), new Integer[] {1}));
  }

  @ParameterizedTest
  @MethodSource("provideExpectedListsAndSplitListsAndAlphabet")
  void shouldSplitSpliterators(
      List<Integer> splitExpectedList, List<Integer> sourceExpectedList, Integer[] alphabet) {
    List<Integer> splitActualList = new ArrayList<>();
    List<Integer> sourceActualList = new ArrayList<>();
    ArrayTapeSpliterator<Integer> sourceSpliterator = new ArrayTapeSpliterator<>(alphabet);
    Spliterator<Integer> splitSpliterator = sourceSpliterator.trySplit();

    splitSpliterator.forEachRemaining(splitActualList::add);
    sourceSpliterator.forEachRemaining(sourceActualList::add);

    Assertions.assertThat(splitActualList).isEqualTo(splitExpectedList);
    Assertions.assertThat(sourceActualList).isEqualTo(sourceExpectedList);
  }

  @ParameterizedTest
  @MethodSource("provideExpectedListsAndSplitListsAndAlphabet")
  void shouldKnowSizeOfSpliterators(
      List<Integer> splitExpectedList, List<Integer> sourceExpectedList, Integer[] alphabet) {
    int expectedSplitSize = splitExpectedList.size();
    int expectedSourceSize = sourceExpectedList.size();
    ArrayTapeSpliterator<Integer> sourceSpliterator = new ArrayTapeSpliterator<>(alphabet);
    Spliterator<Integer> splitSpliterator = sourceSpliterator.trySplit();

    long actualSplitSize = splitSpliterator.getExactSizeIfKnown();
    long actualSourceSize = sourceSpliterator.getExactSizeIfKnown();

    Assertions.assertThat(actualSplitSize).isEqualTo(expectedSplitSize);
    Assertions.assertThat(actualSourceSize).isEqualTo(expectedSourceSize);
  }

  @Test
  void shouldReduceSizeOfSplitSpliteratorAtEachIteration() {
    Integer[] alphabet = {
      null, 1, 0, null, 1, 0, null, null, 1, 0, null, null, null, 1, null, 0, null
    };
    var spliterator1 = new ArrayTapeSpliterator<>(alphabet);

    Assertions.assertThat(spliterator1.getExactSizeIfKnown()).isEqualTo(8);
    spliterator1.tryAdvance(System.out::println);
    Assertions.assertThat(spliterator1.getExactSizeIfKnown()).isEqualTo(7);

    var spliterator2 = spliterator1.trySplit();
    Assertions.assertThat(spliterator1.getExactSizeIfKnown()).isEqualTo(3);
    Assertions.assertThat(spliterator2.getExactSizeIfKnown()).isEqualTo(4);
    spliterator1.tryAdvance(System.out::println);
    spliterator2.tryAdvance(System.out::println);
    Assertions.assertThat(spliterator1.getExactSizeIfKnown()).isEqualTo(2);
    Assertions.assertThat(spliterator2.getExactSizeIfKnown()).isEqualTo(3);

    var spliterator3 = spliterator2.trySplit();
    var spliterator4 = spliterator1.trySplit();
    Assertions.assertThat(spliterator1.getExactSizeIfKnown()).isEqualTo(1);
    Assertions.assertThat(spliterator2.getExactSizeIfKnown()).isEqualTo(1);
    Assertions.assertThat(spliterator3.getExactSizeIfKnown()).isEqualTo(2);
    Assertions.assertThat(spliterator4.getExactSizeIfKnown()).isEqualTo(1);
    spliterator1.tryAdvance(System.out::println);
    spliterator2.tryAdvance(System.out::println);
    spliterator3.tryAdvance(System.out::println);
    spliterator4.tryAdvance(System.out::println);
    Assertions.assertThat(spliterator1.getExactSizeIfKnown()).isEqualTo(0);
    Assertions.assertThat(spliterator2.getExactSizeIfKnown()).isEqualTo(0);
    Assertions.assertThat(spliterator3.getExactSizeIfKnown()).isEqualTo(1);
    Assertions.assertThat(spliterator4.getExactSizeIfKnown()).isEqualTo(0);

    var spliterator5 = spliterator3.trySplit();
    Assertions.assertThat(spliterator1.getExactSizeIfKnown()).isEqualTo(0);
    Assertions.assertThat(spliterator2.getExactSizeIfKnown()).isEqualTo(0);
    Assertions.assertThat(spliterator3.getExactSizeIfKnown()).isEqualTo(1);
    Assertions.assertThat(spliterator4.getExactSizeIfKnown()).isEqualTo(0);
    Assertions.assertThat(spliterator5.getExactSizeIfKnown()).isEqualTo(0);
    spliterator1.tryAdvance(System.out::println);
    spliterator2.tryAdvance(System.out::println);
    spliterator3.tryAdvance(System.out::println);
    spliterator4.tryAdvance(System.out::println);
    spliterator5.tryAdvance(System.out::println);
    Assertions.assertThat(spliterator1.getExactSizeIfKnown()).isEqualTo(0);
    Assertions.assertThat(spliterator2.getExactSizeIfKnown()).isEqualTo(0);
    Assertions.assertThat(spliterator3.getExactSizeIfKnown()).isEqualTo(0);
    Assertions.assertThat(spliterator4.getExactSizeIfKnown()).isEqualTo(0);
    Assertions.assertThat(spliterator5.getExactSizeIfKnown()).isEqualTo(0);
  }
}
