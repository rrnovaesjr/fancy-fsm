package org.fancyfsm.core.api.turing.tapes.iterators;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Spliterator;

@ExtendWith(MockitoExtension.class)
class BiDirectionalArrayTapeSpliteratorTest {

  @Test
  void shouldRunForEachRemainingOnNegativeAndPositiveSpliterators() {
    List<Character> actualCharacters = new ArrayList<>();
    List<Character> expectedCharacters = List.of('O', 'n', 'e', ' ', 'P', 'i', 'e', 'c', 'e');
    var biDirectionalArrayTapeSpliterator =
        new BiDirectionalArrayTapeSpliterator<>(
            new Character[] {
              ' ', null, 'e', null, null, 'n', null, null, null, 'O', null, null, null, null
            },
            new Character[] {
              null, 'P', null, 'i', null, null, 'e', null, null, null, 'c', null, null, null, null,
              'e', null
            });

    biDirectionalArrayTapeSpliterator.forEachRemaining(actualCharacters::add);

    Assertions.assertThat(actualCharacters).isEqualTo(expectedCharacters);
  }

  @Test
  void shouldTryAdvanceOnNegativeTapeOnly() {
    List<Character> actualCharacters = new ArrayList<>();
    List<Character> expectedCharacters = List.of(' ');
    var biDirectionalArrayTapeSpliterator =
        new BiDirectionalArrayTapeSpliterator<>(
            new Character[] {null, ' ', null, null}, new Character[] {});

    boolean actualTryAdvance = biDirectionalArrayTapeSpliterator.tryAdvance(actualCharacters::add);

    Assertions.assertThat(actualCharacters).isEqualTo(expectedCharacters);
    Assertions.assertThat(actualTryAdvance).isTrue();
  }

  @Test
  void shouldTryAdvanceOnPositiveTape() {
    List<Character> actualCharacters = new ArrayList<>();
    List<Character> expectedCharacters = List.of(' ');
    var biDirectionalArrayTapeSpliterator =
        new BiDirectionalArrayTapeSpliterator<>(
            new Character[] {null, null}, new Character[] {null, ' ', null, null});

    boolean actualTryAdvance = biDirectionalArrayTapeSpliterator.tryAdvance(actualCharacters::add);

    Assertions.assertThat(actualCharacters).isEqualTo(expectedCharacters);
    Assertions.assertThat(actualTryAdvance).isTrue();
  }

  @Test
  void shouldNotAdvanceOnAnyOfTheTapes() {
    List<Character> actualCharacters = new ArrayList<>();
    List<Character> expectedCharacters = Collections.emptyList();
    var biDirectionalArrayTapeSpliterator =
        new BiDirectionalArrayTapeSpliterator<>(
            new Character[] {null, null}, new Character[] {null, null, null, null});

    boolean actualTryAdvance = biDirectionalArrayTapeSpliterator.tryAdvance(actualCharacters::add);

    Assertions.assertThat(actualCharacters).isEqualTo(expectedCharacters);
    Assertions.assertThat(actualTryAdvance).isFalse();
  }

  @Test
  void shouldTryToSplitOnNegativeTapeOnly() {
    List<Character> actualCharacters = new ArrayList<>();
    List<Character> expectedSplitCharacters = List.of('a', 'b', 'c', 'd', 'e');
    List<Character> expectedCharacters = List.of('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i');
    var biDirectionalArrayTapeSpliterator =
        new BiDirectionalArrayTapeSpliterator<>(
            new Character[] {'h', 'g', 'f', 'e', 'd', 'c', 'b', 'a'}, new Character[] {null, 'i'});

    Spliterator<Character> actualSplit = biDirectionalArrayTapeSpliterator.trySplit();

    actualSplit.forEachRemaining(actualCharacters::add);

    Assertions.assertThat(actualCharacters).isEqualTo(expectedSplitCharacters);

    biDirectionalArrayTapeSpliterator.forEachRemaining(actualCharacters::add);

    Assertions.assertThat(actualCharacters).isEqualTo(expectedCharacters);
    Assertions.assertThat(actualSplit.tryAdvance(actualCharacters::add)).isFalse();
    Assertions.assertThat(biDirectionalArrayTapeSpliterator.tryAdvance(actualCharacters::add))
        .isFalse();
  }

  @Test
  void shouldTrySplitOnPositiveTape() {
    List<Character> actualCharacters = new ArrayList<>();
    List<Character> expectedSplitCharacters = List.of('a', 'b', 'c', 'd');
    List<Character> expectedCharacters = List.of('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h');
    var biDirectionalArrayTapeSpliterator =
        new BiDirectionalArrayTapeSpliterator<>(
            new Character[] {}, new Character[] {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'});

    Spliterator<Character> actualSplit = biDirectionalArrayTapeSpliterator.trySplit();

    actualSplit.forEachRemaining(actualCharacters::add);

    Assertions.assertThat(actualCharacters).isEqualTo(expectedSplitCharacters);

    biDirectionalArrayTapeSpliterator.forEachRemaining(actualCharacters::add);

    Assertions.assertThat(actualCharacters).isEqualTo(expectedCharacters);
    Assertions.assertThat(actualSplit.tryAdvance(actualCharacters::add)).isFalse();
    Assertions.assertThat(biDirectionalArrayTapeSpliterator.tryAdvance(actualCharacters::add))
        .isFalse();
  }

  @Test
  void shouldTryToSplitOnPositiveAndNegativeTapesInHalf() {
    List<Character> actualCharacters = new ArrayList<>();
    List<Character> expectedSplitCharacters = List.of('a', 'b', 'c', 'd');
    List<Character> expectedCharacters = List.of('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h');
    var biDirectionalArrayTapeSpliterator =
        new BiDirectionalArrayTapeSpliterator<>(
            new Character[] {'d', 'c', 'b', 'a'}, new Character[] {'e', 'f', 'g', 'h'});

    Spliterator<Character> actualSplit = biDirectionalArrayTapeSpliterator.trySplit();

    actualSplit.forEachRemaining(actualCharacters::add);

    Assertions.assertThat(actualCharacters).isEqualTo(expectedSplitCharacters);

    biDirectionalArrayTapeSpliterator.forEachRemaining(actualCharacters::add);

    Assertions.assertThat(actualCharacters).isEqualTo(expectedCharacters);
    Assertions.assertThat(actualSplit.tryAdvance(actualCharacters::add)).isFalse();
    Assertions.assertThat(biDirectionalArrayTapeSpliterator.tryAdvance(actualCharacters::add))
        .isFalse();
  }

  @Test
  void shouldTryToSplitOnPositiveAndNegativeTapesInEvenSides() {
    List<Character> actualCharacters = new ArrayList<>();
    List<Character> expectedSplitCharacters = List.of('a', 'b', 'c', 'd');
    List<Character> expectedCharacters = List.of('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h');
    var biDirectionalArrayTapeSpliterator =
        new BiDirectionalArrayTapeSpliterator<>(
            new Character[] {'c', 'b', 'a'}, new Character[] {'d', 'e', 'f', 'g', 'h'});

    Spliterator<Character> actualSplit = biDirectionalArrayTapeSpliterator.trySplit();

    actualSplit.forEachRemaining(actualCharacters::add);

    Assertions.assertThat(actualCharacters).isEqualTo(expectedSplitCharacters);

    biDirectionalArrayTapeSpliterator.forEachRemaining(actualCharacters::add);

    Assertions.assertThat(actualCharacters).isEqualTo(expectedCharacters);
    Assertions.assertThat(actualSplit.tryAdvance(actualCharacters::add)).isFalse();
    Assertions.assertThat(biDirectionalArrayTapeSpliterator.tryAdvance(actualCharacters::add))
        .isFalse();
  }

  @Test
  void shouldAlsoSplitNullValuesInTwoSidesOfTheTape() {
    List<Character> actualCharacters = new ArrayList<>();
    List<Character> expectedSplitCharacters = List.of('a', 'b');
    List<Character> expectedCharacters = List.of('a', 'b', 'c');
    var biDirectionalArrayTapeSpliterator =
        new BiDirectionalArrayTapeSpliterator<>(
            new Character[] {null, 'a', null}, new Character[] {'b', null, 'c', null, null});

    Spliterator<Character> actualSplit = biDirectionalArrayTapeSpliterator.trySplit();

    actualSplit.forEachRemaining(actualCharacters::add);

    Assertions.assertThat(actualCharacters).isEqualTo(expectedSplitCharacters);

    biDirectionalArrayTapeSpliterator.forEachRemaining(actualCharacters::add);

    Assertions.assertThat(actualCharacters).isEqualTo(expectedCharacters);
    Assertions.assertThat(actualSplit.tryAdvance(actualCharacters::add)).isFalse();
    Assertions.assertThat(biDirectionalArrayTapeSpliterator.tryAdvance(actualCharacters::add))
        .isFalse();
  }

  @Test
  void shouldEstimateSizeOnSplitSpliterators() {
    var biDirectionalArrayTapeSpliterator =
        new BiDirectionalArrayTapeSpliterator<>(
            new Character[] {null, 'a', null}, new Character[] {'b', null, 'c', null, null});

    Spliterator<Character> actualSplit = biDirectionalArrayTapeSpliterator.trySplit();

    long actualSplitSize = actualSplit.getExactSizeIfKnown();
    long actualOriginalSize = biDirectionalArrayTapeSpliterator.getExactSizeIfKnown();

    Assertions.assertThat(actualSplitSize).isEqualTo(2L);
    Assertions.assertThat(actualOriginalSize).isEqualTo(1L);
  }

  @Test
  void shouldReturnCharacteristics() {
    var biDirectionalArrayTapeSpliterator =
        new BiDirectionalArrayTapeSpliterator<>(new Object[] {}, new Object[] {});

    int characteristics = biDirectionalArrayTapeSpliterator.characteristics();

    Assertions.assertThat(characteristics & Spliterator.ORDERED).isEqualTo(0);
    Assertions.assertThat(characteristics & Spliterator.CONCURRENT).isEqualTo(0);
    Assertions.assertThat(characteristics & Spliterator.IMMUTABLE).isEqualTo(0);
    Assertions.assertThat(characteristics & Spliterator.SORTED).isEqualTo(0);
    Assertions.assertThat(characteristics & Spliterator.DISTINCT).isEqualTo(0);
    Assertions.assertThat(characteristics & Spliterator.SIZED).isEqualTo(Spliterator.SIZED);
    Assertions.assertThat(characteristics & Spliterator.SUBSIZED).isEqualTo(Spliterator.SUBSIZED);
    Assertions.assertThat(characteristics & Spliterator.NONNULL).isEqualTo(Spliterator.NONNULL);
  }
}
