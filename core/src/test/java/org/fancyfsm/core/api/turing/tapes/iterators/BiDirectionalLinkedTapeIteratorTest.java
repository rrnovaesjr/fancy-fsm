package org.fancyfsm.core.api.turing.tapes.iterators;

import org.assertj.core.api.Assertions;
import org.fancyfsm.core.api.turing.tapes.BiDirectionalLinkedTape;
import org.fancyfsm.core.api.turing.tapes.commands.HeadPositionSwap;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

class BiDirectionalLinkedTapeIteratorTest {
    @Test
    void shouldConsumeAllRemainingElementsFromBothIterators() {
        List<Character> actualCharacters = new ArrayList<>();
        List<Character> expectedCharacters = List.of('O', 'n', 'e', ' ', 'P', 'i', 'e', 'c', 'e');
        var doubleLinkedTape = new BiDirectionalLinkedTape<Character>();
        doubleLinkedTape.shiftHead(HeadPositionSwap.LEFT);
        doubleLinkedTape.shiftHead(HeadPositionSwap.LEFT);
        doubleLinkedTape.shiftHead(HeadPositionSwap.LEFT);
        doubleLinkedTape.shiftHead(HeadPositionSwap.LEFT);
        doubleLinkedTape.shiftHead(HeadPositionSwap.LEFT);
        doubleLinkedTape.shiftHead(HeadPositionSwap.LEFT);
        doubleLinkedTape.writeToHead(null);
        doubleLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
        doubleLinkedTape.writeToHead('O');
        doubleLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
        doubleLinkedTape.writeToHead('n');
        doubleLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
        doubleLinkedTape.writeToHead('e');
        doubleLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
        doubleLinkedTape.writeToHead(' ');
        doubleLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
        doubleLinkedTape.writeToHead('P');
        doubleLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
        doubleLinkedTape.writeToHead('i');
        doubleLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
        doubleLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
        doubleLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
        doubleLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
        doubleLinkedTape.writeToHead('e');
        doubleLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
        doubleLinkedTape.writeToHead('c');
        doubleLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
        doubleLinkedTape.writeToHead('e');
        doubleLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
        doubleLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
        doubleLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
        doubleLinkedTape.shiftHead(HeadPositionSwap.RIGHT);

        doubleLinkedTape.iterator().forEachRemaining(actualCharacters::add);

        Assertions.assertThat(actualCharacters).isEqualTo(expectedCharacters);
    }

    @Test
    void shouldNotCheckIfPositiveTapeHasNextIfNegativeTapeStillHasNext() {
        var doubleLinkedTape = new BiDirectionalLinkedTape<Character>();
        doubleLinkedTape.shiftHead(HeadPositionSwap.LEFT);
        doubleLinkedTape.shiftHead(HeadPositionSwap.LEFT);
        doubleLinkedTape.shiftHead(HeadPositionSwap.LEFT);
        doubleLinkedTape.shiftHead(HeadPositionSwap.LEFT);
        doubleLinkedTape.shiftHead(HeadPositionSwap.LEFT);
        doubleLinkedTape.shiftHead(HeadPositionSwap.LEFT);
        doubleLinkedTape.writeToHead(' ');

        boolean actualHasNext = doubleLinkedTape.iterator().hasNext();

        Assertions.assertThat(actualHasNext).isTrue();
    }

    @Test
    void shouldReturnTrueWhenPositiveIteratorHasNext() {
        var doubleLinkedTape = new BiDirectionalLinkedTape<Character>();
        doubleLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
        doubleLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
        doubleLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
        doubleLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
        doubleLinkedTape.writeToHead(' ');
        doubleLinkedTape.shiftHead(HeadPositionSwap.LEFT);
        doubleLinkedTape.shiftHead(HeadPositionSwap.LEFT);
        doubleLinkedTape.writeToHead(' ');

        boolean actualHasNext = doubleLinkedTape.iterator().hasNext();

        Assertions.assertThat(actualHasNext).isTrue();
    }

    @Test
    void shouldReturnFalseWhenBothIteratorsHaveNoNext() {
        var doubleLinkedTape = new BiDirectionalLinkedTape<Character>();
        doubleLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
        doubleLinkedTape.shiftHead(HeadPositionSwap.LEFT);
        doubleLinkedTape.shiftHead(HeadPositionSwap.LEFT);

        boolean actualHasNext = doubleLinkedTape.iterator().hasNext();

        Assertions.assertThat(actualHasNext).isFalse();
    }

    @Test
    void shouldReturnNegativeTapeNext() {
        Character expectedNext = ' ';
        var tapeIterator =
                new BiDirectionalArrayTapeIterator<>(
                        new Character[] {null, null, null, ' '}, new Character[] {});

        Character actualNext = tapeIterator.next();

        Assertions.assertThat(actualNext).isEqualTo(expectedNext);
    }

    @Test
    void shouldReturnPositiveTapeNext() {
        Character expectedNext = 'x';
        var doubleLinkedTape = new BiDirectionalLinkedTape<Character>();
        doubleLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
        doubleLinkedTape.shiftHead(HeadPositionSwap.LEFT);
        doubleLinkedTape.shiftHead(HeadPositionSwap.LEFT);
        doubleLinkedTape.writeToHead('x');

        Character actualNext = doubleLinkedTape.iterator().next();

        Assertions.assertThat(actualNext).isEqualTo(expectedNext);
    }

    @Test
    void shouldThrowNoSuchElementExceptionWhenReadingAfterLastIterationOnPositiveTape() {
        var tapeIterator = new BiDirectionalLinkedTape<>().iterator();

        Assertions.assertThatCode(tapeIterator::next)
                .isExactlyInstanceOf(NoSuchElementException.class)
                .hasMessage("The iterator has no more elements");
    }
}
