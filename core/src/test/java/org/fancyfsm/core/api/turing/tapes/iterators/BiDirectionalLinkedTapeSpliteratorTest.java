package org.fancyfsm.core.api.turing.tapes.iterators;

import org.assertj.core.api.Assertions;
import org.fancyfsm.core.api.turing.tapes.BiDirectionalLinkedTape;
import org.fancyfsm.core.api.turing.tapes.commands.HeadPositionSwap;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Spliterator;

public class BiDirectionalLinkedTapeSpliteratorTest {

  @Test
  void shouldRunForEachRemainingOnNegativeAndPositiveSpliterators() {
    List<Character> actualCharacters = new ArrayList<>();
    List<Character> expectedCharacters = List.of('O', 'n', 'e', ' ', 'P', 'i', 'e', 'c', 'e');
    var biDirectionalLinkedTape = new BiDirectionalLinkedTape<Character>();
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);
    biDirectionalLinkedTape.writeToHead('O');
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.writeToHead('n');
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.writeToHead('e');
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.writeToHead(' ');
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.writeToHead('P');
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.writeToHead('i');
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.writeToHead('e');
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.writeToHead('c');
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.writeToHead('e');
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);

    biDirectionalLinkedTape.spliterator().forEachRemaining(actualCharacters::add);

    Assertions.assertThat(actualCharacters).isEqualTo(expectedCharacters);
  }

  @Test
  void shouldTryAdvanceOnNegativeTapeOnly() {
    List<Character> actualCharacters = new ArrayList<>();
    List<Character> expectedCharacters = List.of(' ');
    var biDirectionalLinkedTape = new BiDirectionalLinkedTape<Character>();
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);
    biDirectionalLinkedTape.writeToHead(' ');
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);

    boolean actualTryAdvance =
        biDirectionalLinkedTape.spliterator().tryAdvance(actualCharacters::add);

    Assertions.assertThat(actualCharacters).isEqualTo(expectedCharacters);
    Assertions.assertThat(actualTryAdvance).isTrue();
  }

  @Test
  void shouldTryAdvanceOnPositiveTape() {
    List<Character> actualCharacters = new ArrayList<>();
    List<Character> expectedCharacters = List.of(' ');
    var biDirectionalLinkedTape = new BiDirectionalLinkedTape<Character>();
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.writeToHead(' ');
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);

    boolean actualTryAdvance =
        biDirectionalLinkedTape.spliterator().tryAdvance(actualCharacters::add);

    Assertions.assertThat(actualCharacters).isEqualTo(expectedCharacters);
    Assertions.assertThat(actualTryAdvance).isTrue();
  }

  @Test
  void shouldNotAdvanceOnAnyOfTheTapes() {
    List<Character> actualCharacters = new ArrayList<>();
    List<Character> expectedCharacters = Collections.emptyList();
    var biDirectionalLinkedTape = new BiDirectionalLinkedTape<Character>();
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);

    boolean actualTryAdvance =
        biDirectionalLinkedTape.spliterator().tryAdvance(actualCharacters::add);

    Assertions.assertThat(actualCharacters).isEqualTo(expectedCharacters);
    Assertions.assertThat(actualTryAdvance).isFalse();
  }

  @Test
  void shouldTrySplitTapeInHalf() {
    List<Character> actualCharacters = new ArrayList<>();
    List<Character> expectedSplitCharacters = List.of('a', 'b', 'c', 'd', 'e');
    List<Character> expectedCharacters = List.of('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i');
    var biDirectionalArrayTapeSpliterator =
        new BiDirectionalArrayTapeSpliterator<>(
            new Character[] {'h', 'g', 'f', 'e', 'd', 'c', 'b', 'a'}, new Character[] {null, 'i'});

    Spliterator<Character> actualSplit = biDirectionalArrayTapeSpliterator.trySplit();

    actualSplit.forEachRemaining(actualCharacters::add);

    Assertions.assertThat(actualCharacters).isEqualTo(expectedSplitCharacters);

    biDirectionalArrayTapeSpliterator.forEachRemaining(actualCharacters::add);

    Assertions.assertThat(actualCharacters).isEqualTo(expectedCharacters);
    Assertions.assertThat(actualSplit.tryAdvance(actualCharacters::add)).isFalse();
    Assertions.assertThat(biDirectionalArrayTapeSpliterator.tryAdvance(actualCharacters::add))
        .isFalse();
  }

  @Test
  void shouldTrySplitOnPositiveTape() {
    List<Character> actualCharacters = new ArrayList<>();
    List<Character> expectedSplitCharacters = List.of('a', 'b', 'c', 'd');
    List<Character> expectedCharacters = List.of('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h');
    var biDirectionalArrayTapeSpliterator =
        new BiDirectionalArrayTapeSpliterator<>(
            new Character[] {}, new Character[] {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'});

    Spliterator<Character> actualSplit = biDirectionalArrayTapeSpliterator.trySplit();

    actualSplit.forEachRemaining(actualCharacters::add);

    Assertions.assertThat(actualCharacters).isEqualTo(expectedSplitCharacters);

    biDirectionalArrayTapeSpliterator.forEachRemaining(actualCharacters::add);

    Assertions.assertThat(actualCharacters).isEqualTo(expectedCharacters);
    Assertions.assertThat(actualSplit.tryAdvance(actualCharacters::add)).isFalse();
    Assertions.assertThat(biDirectionalArrayTapeSpliterator.tryAdvance(actualCharacters::add))
        .isFalse();
  }

  @Test
  void shouldTryToSplitOnPositiveAndNegativeTapesInHalf() {
    List<Character> actualCharacters = new ArrayList<>();
    List<Character> expectedSplitCharacters = List.of('a', 'b', 'c', 'd');
    List<Character> expectedCharacters = List.of('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h');
    var biDirectionalArrayTapeSpliterator =
        new BiDirectionalArrayTapeSpliterator<>(
            new Character[] {'d', 'c', 'b', 'a'}, new Character[] {'e', 'f', 'g', 'h'});

    Spliterator<Character> actualSplit = biDirectionalArrayTapeSpliterator.trySplit();

    actualSplit.forEachRemaining(actualCharacters::add);

    Assertions.assertThat(actualCharacters).isEqualTo(expectedSplitCharacters);

    biDirectionalArrayTapeSpliterator.forEachRemaining(actualCharacters::add);

    Assertions.assertThat(actualCharacters).isEqualTo(expectedCharacters);
    Assertions.assertThat(actualSplit.tryAdvance(actualCharacters::add)).isFalse();
    Assertions.assertThat(biDirectionalArrayTapeSpliterator.tryAdvance(actualCharacters::add))
        .isFalse();
  }

  @Test
  void shouldTryToSplitOnPositiveAndNegativeTapesInEvenSides() {
    List<Character> actualCharacters = new ArrayList<>();
    List<Character> expectedSplitCharacters = List.of('a', 'b', 'c', 'd');
    List<Character> expectedCharacters = List.of('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h');
    var biDirectionalLinkedTape = new BiDirectionalLinkedTape<Character>();
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);
    biDirectionalLinkedTape.writeToHead('a');
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.writeToHead('b');
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.writeToHead('c');
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.writeToHead('d');
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.writeToHead('e');
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.writeToHead('f');
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.writeToHead('g');
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.writeToHead('h');
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    var biDirectionalLinkedTapeSpliterator = biDirectionalLinkedTape.spliterator();

    Spliterator<Character> actualSplit = biDirectionalLinkedTapeSpliterator.trySplit();

    actualSplit.forEachRemaining(actualCharacters::add);

    Assertions.assertThat(actualCharacters).isEqualTo(expectedSplitCharacters);

    biDirectionalLinkedTapeSpliterator.forEachRemaining(actualCharacters::add);

    Assertions.assertThat(actualCharacters).isEqualTo(expectedCharacters);
    Assertions.assertThat(actualSplit.tryAdvance(actualCharacters::add)).isFalse();
    Assertions.assertThat(biDirectionalLinkedTapeSpliterator.tryAdvance(actualCharacters::add))
        .isFalse();
  }

  @Test
  void shouldIgnoreNullValuesWhenSplittingTape() {
    List<Character> actualCharacters = new ArrayList<>();
    List<Character> expectedSplitCharacters = List.of('a', 'b', 'c', 'd');
    List<Character> expectedCharacters = List.of('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h');
    var biDirectionalLinkedTape = new BiDirectionalLinkedTape<Character>();
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.LEFT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.writeToHead('a');
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.writeToHead('b');
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.writeToHead('c');
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.writeToHead('d');
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.writeToHead('e');
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.writeToHead('f');
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.writeToHead('g');
    biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    biDirectionalLinkedTape.writeToHead('h');
    var biDirectionalLinkedTapeSpliterator = biDirectionalLinkedTape.spliterator();

    Spliterator<Character> actualSplit = biDirectionalLinkedTapeSpliterator.trySplit();

    actualSplit.forEachRemaining(actualCharacters::add);

    Assertions.assertThat(actualCharacters).isEqualTo(expectedSplitCharacters);

    biDirectionalLinkedTapeSpliterator.forEachRemaining(actualCharacters::add);

    Assertions.assertThat(actualCharacters).isEqualTo(expectedCharacters);
    Assertions.assertThat(actualSplit.tryAdvance(actualCharacters::add)).isFalse();
    Assertions.assertThat(biDirectionalLinkedTapeSpliterator.tryAdvance(actualCharacters::add))
        .isFalse();
  }

  @Test
  void shouldUseBatchSizeIfHugeSpliteratorsAreUsed() {
    long expectedSplitSize = 1 << 25;
    long expectedRemainingSize = ((1 << 25) * 3) - (1 << 25);
    var biDirectionalLinkedTape = new BiDirectionalLinkedTape<Character>();
    for (int i = 0; i < (1 << 25) * 3; i++) {
      biDirectionalLinkedTape.writeToHead('x');
      biDirectionalLinkedTape.shiftHead(HeadPositionSwap.RIGHT);
    }

    var biDirectionalLinkedTapeSpliterator = biDirectionalLinkedTape.spliterator();

    Assertions.assertThat(biDirectionalLinkedTapeSpliterator.trySplit().getExactSizeIfKnown())
        .isEqualTo(expectedSplitSize);
    Assertions.assertThat(biDirectionalLinkedTapeSpliterator.getExactSizeIfKnown())
        .isEqualTo(expectedRemainingSize);
  }

  @Test
  void shouldReturnCharacteristics() {
    int characteristics = new BiDirectionalLinkedTape<>().spliterator().characteristics();

    Assertions.assertThat(characteristics & Spliterator.ORDERED).isEqualTo(0);
    Assertions.assertThat(characteristics & Spliterator.CONCURRENT).isEqualTo(0);
    Assertions.assertThat(characteristics & Spliterator.IMMUTABLE).isEqualTo(0);
    Assertions.assertThat(characteristics & Spliterator.SORTED).isEqualTo(0);
    Assertions.assertThat(characteristics & Spliterator.DISTINCT).isEqualTo(0);
    Assertions.assertThat(characteristics & Spliterator.SIZED).isEqualTo(Spliterator.SIZED);
    Assertions.assertThat(characteristics & Spliterator.SUBSIZED).isEqualTo(Spliterator.SUBSIZED);
    Assertions.assertThat(characteristics & Spliterator.NONNULL).isEqualTo(Spliterator.NONNULL);
  }
}
