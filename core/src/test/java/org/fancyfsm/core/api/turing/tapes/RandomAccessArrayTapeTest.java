package org.fancyfsm.core.api.turing.tapes;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class RandomAccessArrayTapeTest {

  private static Stream<Arguments> provideIndexes() {
    return IntStream.rangeClosed(-10, -5)
        .mapToObj(Arguments::of);
  }

  @ParameterizedTest
  @MethodSource("provideIndexes")
  void shouldNotShiftHeaderToLeftWhenAt0Index(int index) {
    RandomAccessArrayTape<Object> rightInfiniteTape = new RandomAccessArrayTape<>();

    Assertions.assertThatCode(() -> rightInfiniteTape.shiftHead(index))
        .isExactlyInstanceOf(IndexOutOfBoundsException.class)
        .hasMessage("Cannot move to the left of the 0 index");
  }

  @Test
  void shouldExecuteAllSetsOfCommands() {
    RandomAccessArrayTape<String> rightInfiniteTape = new RandomAccessArrayTape<>();

    rightInfiniteTape.writeToHead("Mega Man X");
    rightInfiniteTape.shiftHead(1);
    rightInfiniteTape.writeToHead("Zero");
    rightInfiniteTape.shiftHead(3);
    rightInfiniteTape.writeToHead("Axl");

    Assertions.assertThat(rightInfiniteTape.readFromHead()).isEqualTo("Axl");
    rightInfiniteTape.shiftHead(2);
    Assertions.assertThat(rightInfiniteTape.readFromHead()).isNull();
    rightInfiniteTape.shiftHead(1);
    Assertions.assertThat(rightInfiniteTape.readFromHead()).isEqualTo("Zero");
    rightInfiniteTape.shiftHead(0);
    Assertions.assertThat(rightInfiniteTape.readFromHead()).isEqualTo("Mega Man X");
  }

  @Test
  void shouldWriteEvenBeyondInitialCapacity() {
    RandomAccessArrayTape<Object> rightInfiniteTape = new RandomAccessArrayTape<>(1);

    rightInfiniteTape.shiftHead(2);

    rightInfiniteTape.writeToHead("100");
    Assertions.assertThat(rightInfiniteTape.readFromHead()).isEqualTo("100");

    rightInfiniteTape.shiftHead(7);

    rightInfiniteTape.writeToHead("1012");
    Assertions.assertThat(rightInfiniteTape.readFromHead()).isEqualTo("1012");

    rightInfiniteTape.shiftHead(2);

    Assertions.assertThat(rightInfiniteTape.readFromHead()).isEqualTo("100");
  }

  @Test
  void shouldThrowIllegalArgumentExceptionWhenCommandIsNull() {
    RandomAccessArrayTape<Object> arrayTape = new RandomAccessArrayTape<>();

    Assertions.assertThatCode(() -> arrayTape.shiftHead(null))
        .isExactlyInstanceOf(IllegalArgumentException.class)
        .hasMessage("Command cannot be null");
  }

  private static Stream<Arguments> provideIterationParameters() {
    return Stream.of(
        Arguments.of(
            "One Piece",
            Arrays.asList(
                null, 'O', 'n', 'e', null, ' ', null, null, 'P', null, 'i', null, 'e', 'c', 'e',
                null)));
  }

  @ParameterizedTest
  @MethodSource("provideIterationParameters")
  void shouldAllowToIterateTapeHead(String expectedWord, List<Character> alphabet) {
    RandomAccessArrayTape<Character> rightInfiniteTape = new RandomAccessArrayTape<>();
    int index = 1;
    for (var symbol : alphabet) {
      rightInfiniteTape.writeToHead(symbol);
      rightInfiniteTape.shiftHead(index++);
    }

    StringBuilder actualWord = new StringBuilder();
    for (var symbol : rightInfiniteTape) {
      actualWord.append(symbol);
    }
    Assertions.assertThat(actualWord).hasToString(expectedWord);

    actualWord = new StringBuilder();
    var iterator = rightInfiniteTape.iterator();
    while (iterator.hasNext()) {
      actualWord.append(iterator.next());
    }
    Assertions.assertThat(actualWord).hasToString(expectedWord);

    actualWord = new StringBuilder();
    rightInfiniteTape.stream().forEach(actualWord::append);
    Assertions.assertThat(actualWord).hasToString(expectedWord);
  }
}