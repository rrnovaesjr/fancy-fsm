package org.fancyfsm.core.api.deterministic.states.memory;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class PlainObjectDeterministicStateMemoryTest {

  @Test
  void shouldReplaceLastWrittenValueInMemory() {
    DeterministicStateMemory<String> stateMemory = new PlainObjectDeterministicStateMemory<>();

    Assertions.assertThat(stateMemory.read()).isEmpty();

    stateMemory.write("StateY");
    Assertions.assertThat(stateMemory.read()).hasValue("StateY");

    stateMemory.write("StateX");
    Assertions.assertThat(stateMemory.read()).hasValue("StateX");
  }
}
