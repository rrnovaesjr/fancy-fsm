package org.fancyfsm.core.api.turing.tapes.iterators;

import org.assertj.core.api.Assertions;
import org.assertj.core.api.Condition;
import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;
import java.util.Objects;

class ArrayTapeIteratorTest {

  @Test
  void shouldIterateOverNonNullSymbols() {
    Integer[] alphabet = {null, 1, 0, 0, 1, 0, 0, 1, 0, null, 1, 0, 1, 0, 1, 0, 1, 0, null};
    ArrayTapeIterator<Integer> arrayTapeIterator = new ArrayTapeIterator<>(alphabet);

    Assertions.assertThat(arrayTapeIterator)
        .doesNotHave(new Condition<>(Objects::isNull, "Null elements"));
  }

  @Test
  void shouldGuessNextInOrder() {
    Integer[] alphabet = {
      null, 1, 0, 0, 1, null, null, null, null, null, 1, 0, 1, 0, null, null, null, null, null
    };
    ArrayTapeIterator<Integer> arrayTapeIterator = new ArrayTapeIterator<>(alphabet);

    Assertions.assertThat(arrayTapeIterator).hasNext();
    Assertions.assertThat(arrayTapeIterator.next()).isEqualTo(1);
    Assertions.assertThat(arrayTapeIterator).hasNext();
    Assertions.assertThat(arrayTapeIterator.next()).isEqualTo(0);
    Assertions.assertThat(arrayTapeIterator).hasNext();
    Assertions.assertThat(arrayTapeIterator.next()).isEqualTo(0);
    Assertions.assertThat(arrayTapeIterator).hasNext();
    Assertions.assertThat(arrayTapeIterator.next()).isEqualTo(1);
    Assertions.assertThat(arrayTapeIterator).hasNext();
    Assertions.assertThat(arrayTapeIterator.next()).isEqualTo(1);
    Assertions.assertThat(arrayTapeIterator).hasNext();
    Assertions.assertThat(arrayTapeIterator.next()).isEqualTo(0);
    Assertions.assertThat(arrayTapeIterator).hasNext();
    Assertions.assertThat(arrayTapeIterator.next()).isEqualTo(1);
    Assertions.assertThat(arrayTapeIterator).hasNext();
    Assertions.assertThat(arrayTapeIterator.next()).isEqualTo(0);
    Assertions.assertThat(arrayTapeIterator.hasNext()).isFalse();
  }

  @Test
  void shouldNotHaveNextGivenOnlyNullSymbols() {
    Integer[] alphabet = {null, null, null, null, null};
    ArrayTapeIterator<Integer> arrayTapeIterator = new ArrayTapeIterator<>(alphabet);

    Assertions.assertThat(arrayTapeIterator.hasNext()).isFalse();
  }

  @Test
  void shouldThrowNoSuchElementExceptionWhenUsingNextAndHasNextIsFalse() {
    Integer[] alphabet = {};
    ArrayTapeIterator<Integer> arrayTapeIterator = new ArrayTapeIterator<>(alphabet);

    Assertions.assertThatCode(arrayTapeIterator::next)
        .isExactlyInstanceOf(NoSuchElementException.class)
        .hasMessage("The iterator has no more elements");
  }
}
