package org.fancyfsm.core.api.deterministic.states;

import org.assertj.core.api.Assertions;
import org.fancyfsm.core.api.deterministic.states.memory.DeterministicStateMemory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class DefaultDeterministicStateContextTest {

  Integer initialState = Integer.MIN_VALUE;
  @Mock DeterministicStateMemory<Object> stateMemory;

  @Test
  void shouldThrowNullPointerExceptionWhenStateMemoryIsNull() {
    Assertions.assertThatCode(() -> new DefaultDeterministicStateContext<>(null, null))
        .isExactlyInstanceOf(NullPointerException.class)
        .hasMessage("stateMemory is marked non-null but is null");
  }

  @Test
  void shouldReturnInitialStateIfMemoryIsEmpty() {
    var stateContext = provideStateContext();
    var expectedState = initialState;
    Mockito.when(stateMemory.read()).thenReturn(Optional.empty());

    var actualState = stateContext.read();

    Assertions.assertThat(actualState).isEqualTo(expectedState);
    Mockito.verify(stateMemory, Mockito.times(1)).read();
    Mockito.verifyNoMoreInteractions(stateMemory);
  }

  @Test
  void shouldReturnCurrentStateInMemory() {
    var stateContext = provideStateContext();
    var expectedState = "expected state";
    Mockito.when(stateMemory.read()).thenReturn(Optional.of("expected state"));

    var actualState = stateContext.read();

    Assertions.assertThat(actualState).isEqualTo(expectedState);
    Mockito.verify(stateMemory, Mockito.times(1)).read();
    Mockito.verifyNoMoreInteractions(stateMemory);
  }

  @Test
  void shouldWriteStateToMemory() {
    var stateContext = provideStateContext();

    stateContext.write("expected state");

    Mockito.verify(stateMemory, Mockito.times(1)).write("expected state");
    Mockito.verifyNoMoreInteractions(stateMemory);
  }

  private DeterministicStateContext<Object> provideStateContext() {
    return new DefaultDeterministicStateContext<>(initialState, stateMemory);
  }
}
