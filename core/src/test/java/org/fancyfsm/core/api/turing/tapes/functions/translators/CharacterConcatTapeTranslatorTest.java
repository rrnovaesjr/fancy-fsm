package org.fancyfsm.core.api.turing.tapes.functions.translators;

import java.util.List;
import java.util.function.Supplier;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.assertj.core.api.Assertions;
import org.fancyfsm.core.api.transitions.turing.HeadCommand;
import org.fancyfsm.core.api.transitions.turing.SymbolSupplier;
import org.fancyfsm.core.api.turing.tapes.ArrayTape;
import org.fancyfsm.core.api.turing.tapes.BiDirectionalArrayTape;
import org.fancyfsm.core.api.turing.tapes.BiDirectionalLinkedTape;
import org.fancyfsm.core.api.turing.tapes.ObjectTape;
import org.fancyfsm.core.api.turing.tapes.Tape;
import org.fancyfsm.core.api.turing.tapes.commands.HeadPositionSwap;
import org.fancyfsm.core.api.turing.tapes.functions.Translator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CharacterConcatTapeTranslatorTest {

  Translator<String> translator;

  @BeforeEach
  void beforeEach() {
    translator = new CharacterConcatTapeTranslator();
  }

  @Test
  void shouldWriteOnePieceToArrayTape() {
    List<TestCommand<Character, HeadPositionSwap>> transitions =
        List.of(
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>('O', HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>('n', HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>('e', HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>('P', HeadPositionSwap.LEFT),
            new TestCommand<>(null, HeadPositionSwap.LEFT),
            new TestCommand<>(null, HeadPositionSwap.LEFT),
            new TestCommand<>(' ', HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>('P', HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>('i', HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>('e', HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>('c', HeadPositionSwap.RIGHT),
            new TestCommand<>('e', HeadPositionSwap.RIGHT));
    var tape = generateTape(ArrayTape::new, transitions);

    Assertions.assertThat(translator.accepts(tape)).isTrue();
    Assertions.assertThat(translator.translate(tape)).isEqualTo("One Piece");
  }

  @Test
  void shouldWriteOnePieceToBiDirectionalArrayTape() {
    List<TestCommand<Character, HeadPositionSwap>> transitions =
        List.of(
            new TestCommand<>(null, HeadPositionSwap.LEFT),
            new TestCommand<>(null, HeadPositionSwap.LEFT),
            new TestCommand<>(null, HeadPositionSwap.LEFT),
            new TestCommand<>(null, HeadPositionSwap.LEFT),
            new TestCommand<>(null, HeadPositionSwap.LEFT),
            new TestCommand<>(null, HeadPositionSwap.LEFT),
            new TestCommand<>('O', HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>('n', HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>('e', HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>('P', HeadPositionSwap.LEFT),
            new TestCommand<>(null, HeadPositionSwap.LEFT),
            new TestCommand<>(null, HeadPositionSwap.LEFT),
            new TestCommand<>(' ', HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>('P', HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>('i', HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>('e', HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>('c', HeadPositionSwap.RIGHT),
            new TestCommand<>('e', HeadPositionSwap.RIGHT));
    var tape = generateTape(BiDirectionalArrayTape::new, transitions);

    Assertions.assertThat(translator.accepts(tape)).isTrue();
    Assertions.assertThat(translator.translate(tape)).isEqualTo("One Piece");
  }

  @Test
  void shouldWriteOnePieceToBiDirectionalLinkedTape() {
    List<TestCommand<Character, HeadPositionSwap>> transitions =
        List.of(
            new TestCommand<>(null, HeadPositionSwap.LEFT),
            new TestCommand<>(null, HeadPositionSwap.LEFT),
            new TestCommand<>(null, HeadPositionSwap.LEFT),
            new TestCommand<>(null, HeadPositionSwap.LEFT),
            new TestCommand<>(null, HeadPositionSwap.LEFT),
            new TestCommand<>(null, HeadPositionSwap.LEFT),
            new TestCommand<>('O', HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>('n', HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>('e', HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>('P', HeadPositionSwap.LEFT),
            new TestCommand<>(null, HeadPositionSwap.LEFT),
            new TestCommand<>(null, HeadPositionSwap.LEFT),
            new TestCommand<>(' ', HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>('P', HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>('i', HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>('e', HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>('c', HeadPositionSwap.RIGHT),
            new TestCommand<>('e', HeadPositionSwap.RIGHT));
    var tape = generateTape(BiDirectionalLinkedTape::new, transitions);

    Assertions.assertThat(translator.accepts(tape)).isTrue();
    Assertions.assertThat(translator.translate(tape)).isEqualTo("One Piece");
  }

  @Test
  void shouldNotAcceptTapeIfTypesDoNotMatch() {
    List<TestCommand<Integer, HeadPositionSwap>> transitions =
        List.of(
            new TestCommand<>(null, HeadPositionSwap.LEFT),
            new TestCommand<>(null, HeadPositionSwap.LEFT),
            new TestCommand<>(null, HeadPositionSwap.LEFT),
            new TestCommand<>(null, HeadPositionSwap.LEFT),
            new TestCommand<>(null, HeadPositionSwap.LEFT),
            new TestCommand<>(null, HeadPositionSwap.LEFT),
            new TestCommand<>(1, HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>(0, HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>(0, HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>(null, HeadPositionSwap.RIGHT),
            new TestCommand<>(1, HeadPositionSwap.LEFT));
    var tape = generateTape(BiDirectionalLinkedTape::new, transitions);

    Assertions.assertThat(translator.accepts(tape)).isFalse();
  }

  @Test
  void shouldNotAcceptNotIterableTape() {
    final class Address {
      private String street;
      private String country;
    }
    Address address = new Address();
    List<TestCommand<Object, String>> transitions =
        List.of(new TestCommand<>("BR", "street"), new TestCommand<>("Av. Amazonas", "country"));
    var tape = generateTape(() -> new ObjectTape<>(address, "country"), transitions);

    Assertions.assertThat(translator.accepts(tape)).isFalse();
  }

  private <A, D> Tape<A, D> generateTape(
      Supplier<Tape<A, D>> tapeSupplier, List<TestCommand<A, D>> testCommands) {

    var tape = tapeSupplier.get();
    testCommands.forEach(
        action -> {
          tape.writeToHead(action.newSymbol());
          tape.shiftHead(action.direction());
        });
    return tape;
  }

  @Accessors(fluent = true)
  @Getter
  @RequiredArgsConstructor
  @EqualsAndHashCode
  @ToString
  private static class TestCommand<A, D> implements SymbolSupplier<A>, HeadCommand<D> {
    private final A newSymbol;
    private final D direction;
  }
}
