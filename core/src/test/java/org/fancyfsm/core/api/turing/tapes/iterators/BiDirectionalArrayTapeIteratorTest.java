package org.fancyfsm.core.api.turing.tapes.iterators;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@ExtendWith(MockitoExtension.class)
class BiDirectionalArrayTapeIteratorTest {

  @Test
  void shouldConsumeAllRemainingElementsFromBothIterators() {
    List<Character> actualCharacters = new ArrayList<>();
    List<Character> expectedCharacters = List.of('O', 'n', 'e', ' ', 'P', 'i', 'e', 'c', 'e');
    var tapeIterator =
        new BiDirectionalArrayTapeIterator<>(
            new Character[] {
              ' ', null, 'e', null, null, 'n', null, null, null, 'O', null, null, null, null
            },
            new Character[] {
              null, 'P', null, 'i', null, null, 'e', null, null, null, 'c', null, null, null, null,
              'e', null
            });

    tapeIterator.forEachRemaining(actualCharacters::add);

    Assertions.assertThat(actualCharacters).isEqualTo(expectedCharacters);
  }

  @Test
  void shouldNotCheckIfPositiveTapeHasNextIfNegativeTapeStillHasNext() {
    var tapeIterator =
        new BiDirectionalArrayTapeIterator<>(
            new Character[] {null, null, null, ' '}, new Character[] {});

    boolean actualHasNext = tapeIterator.hasNext();

    Assertions.assertThat(actualHasNext).isTrue();
  }

  @Test
  void shouldReturnTrueWhenPositiveIteratorHasNext() {
    var tapeIterator =
        new BiDirectionalArrayTapeIterator<>(new Character[] {}, new Character[] {'x'});

    boolean actualHasNext = tapeIterator.hasNext();

    Assertions.assertThat(actualHasNext).isTrue();
  }

  @Test
  void shouldReturnFalseWhenBothIteratorsHaveNoNext() {
    var tapeIterator = new BiDirectionalArrayTapeIterator<>(new Character[] {}, new Character[] {});
    boolean actualHasNext = tapeIterator.hasNext();

    Assertions.assertThat(actualHasNext).isFalse();
  }

  @Test
  void shouldReturnNegativeTapeNext() {
    Character expectedNext = ' ';
    var tapeIterator =
        new BiDirectionalArrayTapeIterator<>(
            new Character[] {null, null, null, ' '}, new Character[] {});

    Character actualNext = tapeIterator.next();

    Assertions.assertThat(actualNext).isEqualTo(expectedNext);
  }

  @Test
  void shouldReturnPositiveTapeNext() {
    Character expectedNext = 'x';
    var tapeIterator =
        new BiDirectionalArrayTapeIterator<>(
            new Character[] {null, null, null, null}, new Character[] {null, null, 'x', null});

    Character actualNext = tapeIterator.next();

    Assertions.assertThat(actualNext).isEqualTo(expectedNext);
  }

  @Test
  void shouldThrowNoSuchElementExceptionWhenReadingAfterLastIterationOnPositiveTape() {
    var tapeIterator = new BiDirectionalArrayTapeIterator<>(new Character[] {}, new Character[] {});

    Assertions.assertThatCode(tapeIterator::next)
        .isExactlyInstanceOf(NoSuchElementException.class)
        .hasMessage("The iterator has no more elements");
  }
}
