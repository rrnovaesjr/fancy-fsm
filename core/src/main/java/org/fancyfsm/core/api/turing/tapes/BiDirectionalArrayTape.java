package org.fancyfsm.core.api.turing.tapes;

import org.fancyfsm.core.api.turing.tapes.commands.HeadPositionSwap;
import org.fancyfsm.core.api.turing.tapes.iterators.BiDirectionalArrayTapeIterator;
import org.fancyfsm.core.api.turing.tapes.iterators.BiDirectionalArrayTapeSpliterator;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public class BiDirectionalArrayTape<A> implements IterableTape<A, HeadPositionSwap> {

  @SuppressWarnings("unchecked")
  private final A[][] tape = (A[][]) new Object[2][];

  private int headIndex = 0;

  @SuppressWarnings("unchecked")
  public BiDirectionalArrayTape(int leftInitialCapacity, int rightInitialCapacity) {
    tape[1] = (A[]) new Object[rightInitialCapacity];
    tape[0] = (A[]) new Object[leftInitialCapacity];
  }

  public BiDirectionalArrayTape() {
    this(256, 256);
  }

  @Override
  public A readFromHead() {
    final int side = 1 & (headIndex >> 31);
    final int sidedTapeHeader = removeComplementOfTwoFromTapeHeader();
    adjustCapacity(side, sidedTapeHeader);

    return tape[side][sidedTapeHeader];
  }

  @Override
  public void writeToHead(A symbol) {
    final int side = 1 & (headIndex >> 31);
    final int sidedTapeHeader = removeComplementOfTwoFromTapeHeader();
    adjustCapacity(side, sidedTapeHeader);

    tape[side][sidedTapeHeader] = symbol;
  }

  @Override
  public void shiftHead(HeadPositionSwap command) {
    if (command == null) throw new IllegalArgumentException("Command cannot be null");

    if (command == HeadPositionSwap.LEFT) {
      headIndex--;
      return;
    }

    headIndex++;
  }

  private int removeComplementOfTwoFromTapeHeader() {
    if (headIndex >= 0) return headIndex;
    return -headIndex - 1;
  }

  private void adjustCapacity(int side, int absoluteTapeHeader) {
    if (absoluteTapeHeader < tape[side].length) return;

    tape[side] = Arrays.copyOf(tape[side], 2 * absoluteTapeHeader + 1);
  }

  @Override
  public void forEach(Consumer<? super A> action) {
    for (int i = 1; i >= 0; i--) for (int j = 0; j < tape[i].length; j++) action.accept(tape[i][j]);
  }

  @Override
  public Spliterator<A> spliterator() {
    return new BiDirectionalArrayTapeSpliterator<>(tape[1], tape[0]);
  }

  @Override
  public Iterator<A> iterator() {
    return new BiDirectionalArrayTapeIterator<>(tape[1], tape[0]);
  }
}
