package org.fancyfsm.core.api.turing.tapes;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.fancyfsm.core.api.turing.tapes.commands.HeadPositionSwap;
import org.fancyfsm.core.api.turing.tapes.iterators.ArrayTapeSpliterator;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;

@Accessors(fluent = true)
public class BiDirectionalLinkedTape<A> implements IterableTape<A, HeadPositionSwap> {

  private Node leftestNode;
  private Node head;
  @Getter private int size;

  public BiDirectionalLinkedTape() {
    head = new Node();
    this.leftestNode = head;
    size = 0;
  }

  @Override
  @SuppressWarnings("unchecked")
  public A readFromHead() {
    return (A) head.element;
  }

  @Override
  public void writeToHead(A symbol) {
    if (symbol != null && head.element == null) size++;
    else if (symbol == null && head.element != null) size--;
    head.element = symbol;
  }

  @Override
  public void shiftHead(HeadPositionSwap command) {
    if (command == null) throw new IllegalArgumentException("Command cannot be null");

    if (command == HeadPositionSwap.LEFT) {
      doShift(head.prev, command);
      return;
    }
    doShift(head.next, command);
  }

  private void doShift(Node node, HeadPositionSwap command) {
    if (node == null) {
      node = new Node();

      if (command == HeadPositionSwap.LEFT) {
        node.next = head;
        var headerPrev = head.prev;
        head.prev = node;
        node.prev = headerPrev;
        leftestNode = node;
      } else {
        node.prev = head;
        var headerNext = head.next;
        head.next = node;
        node.next = headerNext;
      }
    }

    head = node;
  }

  @Override
  @SuppressWarnings("unchecked")
  public void forEach(Consumer<? super A> action) {
    for (var index = leftestNode; index != null; index = index.next)
      if (index.element != null) action.accept((A) index.element);
  }

  @Override
  public Spliterator<A> spliterator() {
    return new BiDirectionalLinkedTapeSpliterator<A>(size, 0, leftestNode);
  }

  @Override
  public Iterator<A> iterator() {
    return new BiDirectionalLinkedTapeIterator<A>(leftestNode);
  }

  @NoArgsConstructor(access = AccessLevel.PRIVATE)
  static final class Node {
    private Object element;
    private Node next;
    private Node prev;
  }

  @AllArgsConstructor
  private static class BiDirectionalLinkedTapeIterator<A> implements Iterator<A> {

    private Node index;

    @Override
    public boolean hasNext() {
      while (index != null && index.element == null) index = index.next;
      return index != null;
    }

    @Override
    @SuppressWarnings("unchecked")
    public A next() {
      if (!hasNext()) throw new NoSuchElementException("The iterator has no more elements");
      var symbol = (A) index.element;
      index = index.next;
      return symbol;
    }
  }

  @AllArgsConstructor
  private static class BiDirectionalLinkedTapeSpliterator<A> implements Spliterator<A> {

    private static final int BATCH_SIZE = 1 << 25;
    private final long size;
    private long advancedItems;
    private Node index;

    @Override
    @SuppressWarnings("unchecked")
    public boolean tryAdvance(Consumer<? super A> action) {
      advanceOverNullElements();
      if (index != null) {
        action.accept((A) index.element);
        advancedItems++;
        index = index.next;
        return true;
      }
      return false;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Spliterator<A> trySplit() {
      if (index == null) return Spliterators.emptySpliterator();

      final int subSpliteratorSize = Integer.min((int) ((advancedItems + size) / 2), BATCH_SIZE);

      @RequiredArgsConstructor
      final class SubSpliteratorConsumer implements Consumer<A> {
        final A[] subSpliteratorContainer;
        private int index = 0;

        @Override
        public void accept(A symbol) {
          subSpliteratorContainer[index++] = symbol;
        }
      }

      final var consumer = new SubSpliteratorConsumer((A[]) new Object[subSpliteratorSize]);

      int i = 0;
      while (i < subSpliteratorSize) {
        if (index.element != null) {
          consumer.accept((A) index.element);
          i++;
        }
        index = index.next;
      }

      advancedItems += subSpliteratorSize;

      return new ArrayTapeSpliterator<>(consumer.subSpliteratorContainer);
    }

    @Override
    public long estimateSize() {
      return size - advancedItems;
    }

    @Override
    public int characteristics() {
      return SIZED | SUBSIZED | NONNULL;
    }

    private void advanceOverNullElements() {
      while (index != null && index.element == null) index = index.next;
    }
  }
}
