package org.fancyfsm.core.api.deterministic.turing.transitions;

import org.fancyfsm.core.api.deterministic.transitions.DeterministicTransitionImage;
import org.fancyfsm.core.api.transitions.turing.BaseTuringTransition;

public interface DeterministicTuringTransition<S, A, D>
    extends BaseTuringTransition<S, A, D>, DeterministicTransitionImage<S> {}
