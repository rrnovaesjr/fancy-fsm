package org.fancyfsm.core.api.transitions;

public interface TransitionDomain<S, A> {
  S sourceState();

  A symbol();
}
