package org.fancyfsm.core.api;

public record StateSymbolPair<S, A>(S state, A symbol) {

}
