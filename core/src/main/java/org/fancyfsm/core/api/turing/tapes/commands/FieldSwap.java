package org.fancyfsm.core.api.turing.tapes.commands;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

@Accessors(fluent = true)
@Getter
@RequiredArgsConstructor
public class FieldSwap {
  private final String fieldName;
}
