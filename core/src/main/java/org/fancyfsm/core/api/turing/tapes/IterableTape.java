package org.fancyfsm.core.api.turing.tapes;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public interface IterableTape<A, D> extends Tape<A, D>, Iterable<A> {

  default Stream<A> stream() {
    return StreamSupport.stream(spliterator(), false);
  }

  default Stream<A> parallelStream() {
    return StreamSupport.stream(spliterator(), true);
  }
}
