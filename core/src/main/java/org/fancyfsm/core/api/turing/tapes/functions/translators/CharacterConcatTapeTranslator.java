package org.fancyfsm.core.api.turing.tapes.functions.translators;

import java.util.stream.Collector;

public class CharacterConcatTapeTranslator
    extends CollectableAlphabetTranslator<Character, String> {

  public CharacterConcatTapeTranslator() {
    super(
        Character.class,
        Collector.of(
            StringBuilder::new,
            StringBuilder::append,
            StringBuilder::append,
            StringBuilder::toString));
  }
}
