package org.fancyfsm.core.api.deterministic.transitions;

public interface DeterministicTransitionImage<S> {
  S nextState();
}
