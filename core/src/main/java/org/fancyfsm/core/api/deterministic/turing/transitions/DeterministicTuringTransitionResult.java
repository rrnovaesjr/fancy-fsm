package org.fancyfsm.core.api.deterministic.turing.transitions;

public record DeterministicTuringTransitionResult<S, A, D>(S nextState, A symbol, D direction) {

}
