package org.fancyfsm.core.api.turing.tapes;

import java.lang.reflect.Field;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
public class ObjectTape<O> implements Tape<Object, String> {

  private final O object;
  private String currentFieldName;

  private final FieldUtility fieldUtility = new FieldUtility();

  @Override
  public Object readFromHead() {
    return fieldUtility.getValueInField();
  }

  @Override
  public void writeToHead(Object symbol) {
    fieldUtility.setValueInField(symbol);
  }

  @Override
  public void shiftHead(String fieldName) {
    currentFieldName = fieldName;
  }

  @NoArgsConstructor(access = AccessLevel.PRIVATE)
  private class FieldUtility {

    public Field getField() {
      try {
        return object.getClass().getDeclaredField(currentFieldName);
      } catch (NoSuchFieldException e) {
        throw new RuntimeException(e);
      }
    }

    public Object getValueInField() {
      try {
        return getField().get(object);
      } catch (IllegalAccessException e) {
        throw new RuntimeException(e);
      }
    }

    public void setValueInField(Object value) {
      try {
        var field = getField();
        field.setAccessible(true);
        field.set(object, value);
      } catch (IllegalAccessException e) {
        throw new RuntimeException(e);
      }
    }
  }
}
