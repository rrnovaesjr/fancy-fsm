package org.fancyfsm.core.api.transitions.turing;

import org.fancyfsm.core.api.transitions.TransitionDomain;

public interface BaseTuringTransition<S, A, D>
    extends TransitionDomain<S, A>, SymbolSupplier<A>, HeadCommand<D> {}
