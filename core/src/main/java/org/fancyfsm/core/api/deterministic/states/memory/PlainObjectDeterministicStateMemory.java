package org.fancyfsm.core.api.deterministic.states.memory;

import lombok.NoArgsConstructor;

import java.util.Optional;

@NoArgsConstructor
public class PlainObjectDeterministicStateMemory<S> implements DeterministicStateMemory<S> {
  private S currentState = null;

  @Override
  public Optional<S> read() {
    return Optional.ofNullable(currentState);
  }

  @Override
  public void write(S state) {
    this.currentState = state;
  }
}
