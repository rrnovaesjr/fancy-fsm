package org.fancyfsm.core.api.turing.tapes.iterators;

import java.util.Arrays;
import java.util.Objects;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;

public class BiDirectionalArrayTapeSpliterator<A> implements Spliterator<A> {

  private final A[][] tape;
  private int index;
  private long nonNullElementsCountCache = -1L;
  private long advancedItems = 0L;

  @SuppressWarnings("unchecked")
  public BiDirectionalArrayTapeSpliterator(A[] negativeTape, A[] positiveTape) {
    tape = (A[][]) new Object[2][];
    tape[1] = Objects.requireNonNull(negativeTape);
    tape[0] = Objects.requireNonNull(positiveTape);
    index = -negativeTape.length;
  }

  @Override
  public boolean tryAdvance(Consumer<? super A> action) {
    adjustIndex();
    if (index >= tape[0].length) return false;
    action.accept(getSymbolOnCurrentIndex(index++));
    advancedItems++;
    return true;
  }

  @Override
  @SuppressWarnings("unchecked")
  public Spliterator<A> trySplit() {
    adjustIndex();

    if (index >= tape[0].length) return Spliterators.emptySpliterator();

    int mid = (tape[0].length + index) / 2;

    int tapeIndex = tape(index);

    Spliterator<A> subSpliterator;
    if (tapeIndex == tape(mid)) {
      int left = indexOf(index);
      var chosenTape = tape[tapeIndex];

      if (tapeIndex == 0) {
        subSpliterator = new ArrayTapeSpliterator<>(Arrays.copyOfRange(chosenTape, left, mid));
      } else {
        subSpliterator =
            new BiDirectionalArrayTapeSpliterator<>(
                Arrays.copyOfRange(chosenTape, indexOf(mid - 1), left + 1), (A[]) new Object[] {});
      }
    } else {
      subSpliterator =
          new BiDirectionalArrayTapeSpliterator<>(
              Arrays.copyOfRange(tape[1], 0, indexOf(index) + 1),
              Arrays.copyOfRange(tape[0], 0, indexOf(mid)));
    }

    advancedItems += subSpliterator.estimateSize();
    index = mid;

    return subSpliterator;
  }

  @Override
  public long estimateSize() {
    if (nonNullElementsCountCache == -1)
      nonNullElementsCountCache = determineNonNullElementsCount(tape[0].length);

    return nonNullElementsCountCache - advancedItems;
  }

  private long determineNonNullElementsCount(int endIndex) {
    long nonNullElementsCount = 0L;
    for (int i = -tape[1].length; i < endIndex; i++)
      if (getSymbolOnCurrentIndex(i) != null) nonNullElementsCount++;
    return nonNullElementsCount;
  }

  private void adjustIndex() {
    while (index < tape[0].length && getSymbolOnCurrentIndex(index) == null) index++;
  }

  @Override
  public int characteristics() {
    return NONNULL | SIZED | SUBSIZED;
  }

  private A getSymbolOnCurrentIndex(int index) {
    return tape[tape(index)][indexOf(index)];
  }

  private static int indexOf(int index) {
    return index < 0 ? -index - 1 : index;
  }

  private static int tape(int index) {
    return 1 & (index >> 31);
  }
}
