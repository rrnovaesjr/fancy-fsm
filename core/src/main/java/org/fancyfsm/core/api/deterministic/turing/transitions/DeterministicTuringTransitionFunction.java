package org.fancyfsm.core.api.deterministic.turing.transitions;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.fancyfsm.core.api.StateSymbolPair;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Accessors(fluent = true)
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public class DeterministicTuringTransitionFunction<S, A, D> {

  @Getter(AccessLevel.NONE)
  @NonNull
  private final Map<StateSymbolPair<S, A>, DeterministicTuringTransition<S, A, D>> transitionMap;

  public DeterministicTuringTransitionFunction(
      Set<DeterministicTuringTransition<S, A, D>> transitions) {

    this(
        Objects.requireNonNull(transitions, "Transitions set cannot be null").stream()
            .collect(
                Collectors.toMap(
                    transition ->
                        new StateSymbolPair<>(transition.sourceState(), transition.symbol()),
                    transition -> transition)));
  }

  public Optional<DeterministicTuringTransitionResult<S, A, D>> compute(S sourceState, A symbol) {

    return findTransition(sourceState, symbol).map(this::calculateResult);
  }

  private DeterministicTuringTransitionResult<S, A, D> calculateResult(
      DeterministicTuringTransition<S, A, D> transition) {

    return new DeterministicTuringTransitionResult<>(
        transition.nextState(), transition.newSymbol(), transition.direction());
  }

  public boolean hasTransition(S sourceState, A symbol) {
    return findTransition(sourceState, symbol).isPresent();
  }

  private Optional<DeterministicTuringTransition<S, A, D>> findTransition(S sourceState, A symbol) {
    return Optional.ofNullable(transitionMap.get(new StateSymbolPair<>(sourceState, symbol)));
  }
}
