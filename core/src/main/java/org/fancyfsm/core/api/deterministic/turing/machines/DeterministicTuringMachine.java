package org.fancyfsm.core.api.deterministic.turing.machines;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.experimental.Accessors;
import org.fancyfsm.core.api.deterministic.states.DeterministicStateContext;
import org.fancyfsm.core.api.turing.tapes.Tape;
import org.fancyfsm.core.api.deterministic.turing.transitions.DeterministicTuringTransitionFunction;
import org.fancyfsm.core.api.deterministic.turing.transitions.DeterministicTuringTransitionResult;

import java.util.Optional;
import java.util.Set;

@Getter
@Accessors(fluent = true)
@AllArgsConstructor
public class DeterministicTuringMachine<S, A, D> {

  @NonNull private final DeterministicStateContext<S> stateMemory;
  @NonNull private final Tape<A, D> tape;
  @NonNull private final Set<S> acceptanceStates;
  @NonNull private final DeterministicTuringTransitionFunction<S, A, D> transitionFunction;

  public S currentState() {
    return stateMemory.read();
  }

  public boolean isStopped() {
    return !transitionFunction.hasTransition(currentState(), tape.readFromHead());
  }

  public boolean isAccepted() {
    return isStopped() && acceptanceStates.contains(currentState());
  }

  public void compute() {
    Optional<DeterministicTuringTransitionResult<S, A, D>> transitionResult;
    do {
      final var currentState = currentState();
      transitionResult = transitionFunction.compute(currentState, tape.readFromHead());
      transitionResult.ifPresent(this::onValidTransitionResult);
    } while (transitionResult.isPresent());
  }

  private void onValidTransitionResult(
      DeterministicTuringTransitionResult<S, A, D> transitionResult) {
    final S nextState = transitionResult.nextState();

    stateMemory.write(nextState);
    tape.writeToHead(transitionResult.symbol());
    tape.shiftHead(transitionResult.direction());
  }
}
