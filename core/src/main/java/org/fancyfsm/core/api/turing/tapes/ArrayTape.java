package org.fancyfsm.core.api.turing.tapes;

import org.fancyfsm.core.api.turing.tapes.commands.HeadPositionSwap;
import org.fancyfsm.core.api.turing.tapes.iterators.ArrayTapeIterator;
import org.fancyfsm.core.api.turing.tapes.iterators.ArrayTapeSpliterator;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public class ArrayTape<A> implements IterableTape<A, HeadPositionSwap> {

  private A[] tape;
  private int headIndex = 0;

  @SuppressWarnings("unchecked")
  public ArrayTape(int initialCapacity) {
    this.tape = (A[]) new Object[initialCapacity];
  }

  public ArrayTape() {
    this(256);
  }

  @Override
  public A readFromHead() {
    adjustCapacity();
    return tape[headIndex];
  }

  @Override
  public void writeToHead(A symbol) {
    adjustCapacity();
    tape[headIndex] = symbol;
  }

  @Override
  public void shiftHead(HeadPositionSwap command) {
    if (command == null) throw new IllegalArgumentException("Command cannot be null");

    if (command == HeadPositionSwap.LEFT) {
      if (headIndex == 0)
        throw new IndexOutOfBoundsException("Cannot move to the left of the 0 index");

      headIndex--;
      return;
    }

    headIndex++;
  }

  private void adjustCapacity() {
    if (headIndex < tape.length) return;
    this.tape = Arrays.copyOf(tape, 2 * headIndex + 1);
  }

  @Override
  public Iterator<A> iterator() {
    return new ArrayTapeIterator<>(tape);
  }

  @Override
  public void forEach(Consumer<? super A> action) {
      for (A a : tape) if (a != null) action.accept(a);
  }

  @Override
  public Spliterator<A> spliterator() {
    return new ArrayTapeSpliterator<>(tape);
  }
}
