package org.fancyfsm.core.api.turing.tapes.functions.translators;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.fancyfsm.core.api.turing.tapes.IterableTape;
import org.fancyfsm.core.api.turing.tapes.Tape;
import org.fancyfsm.core.api.turing.tapes.functions.Translator;

import java.util.stream.Stream;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class AbstractIterableTapeTranslator<A, W> implements Translator<W> {

    private final Class<A> symbolClass;

    @Override
    public boolean accepts(Tape<?, ?> tape) {
        if (tape == null)
            return false;

        if (!(tape instanceof IterableTape<?, ?> iterableTape))
            return false;

        return iterableTape.stream()
                .allMatch(symbolClass::isInstance);
    }

    @Override
    public W translate(Tape<?, ?> tape) {

        if (!(tape instanceof IterableTape))
            throw new IllegalArgumentException();

        var tapeStream = ((IterableTape<?, ?>) tape).stream()
                .map(symbolClass::cast);

        return doTranslate(tapeStream);
    }
    
    protected abstract W doTranslate(Stream<A> tapeStream);
}
