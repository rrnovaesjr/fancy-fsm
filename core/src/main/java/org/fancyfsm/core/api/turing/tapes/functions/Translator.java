package org.fancyfsm.core.api.turing.tapes.functions;

import org.fancyfsm.core.api.turing.tapes.Tape;

public interface Translator<W> {

  boolean accepts(Tape<?, ?> tape);

  W translate(Tape<?, ?> tape);
}
