package org.fancyfsm.core.api.turing.tapes.commands;

public enum HeadPositionSwap {
  LEFT,
  RIGHT;
}
