package org.fancyfsm.core.api.turing.tapes;

public interface Tape<A, D> {

  A readFromHead();

  void writeToHead(A symbol);

  void shiftHead(D command);
}
