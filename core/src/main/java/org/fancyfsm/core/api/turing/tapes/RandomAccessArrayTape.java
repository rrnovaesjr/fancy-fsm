package org.fancyfsm.core.api.turing.tapes;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;
import org.fancyfsm.core.api.turing.tapes.iterators.ArrayTapeIterator;
import org.fancyfsm.core.api.turing.tapes.iterators.ArrayTapeSpliterator;

public class RandomAccessArrayTape<A> implements IterableTape<A, Integer> {

  private A[] tape;
  private int headIndex = 0;

  @SuppressWarnings("unchecked")
  public RandomAccessArrayTape(int initialCapacity) {
    this.tape = (A[]) new Object[initialCapacity];
  }

  public RandomAccessArrayTape() {
    this(256);
  }

  @Override
  public A readFromHead() {
    adjustCapacity();
    return tape[headIndex];
  }

  @Override
  public void writeToHead(A symbol) {
    adjustCapacity();
    tape[headIndex] = symbol;
  }

  @Override
  public void shiftHead(Integer index) {
    if (index == null) throw new IllegalArgumentException("Command cannot be null");
    if (index < 0) throw new IndexOutOfBoundsException("Cannot move to the left of the 0 index");

    headIndex = index;
  }

  private void adjustCapacity() {
    if (headIndex < tape.length) return;
    this.tape = Arrays.copyOf(tape, 2 * headIndex + 1);
  }

  @Override
  public void forEach(Consumer<? super A> action) {
    for (A a : tape) if (a != null) action.accept(a);
  }

  @Override
  public Spliterator<A> spliterator() {
    return new ArrayTapeSpliterator<>(tape);
  }

  @Override
  public Iterator<A> iterator() {
    return new ArrayTapeIterator<>(tape);
  }
}
