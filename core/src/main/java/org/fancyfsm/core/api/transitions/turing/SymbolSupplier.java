package org.fancyfsm.core.api.transitions.turing;

public interface SymbolSupplier<A> {
  A newSymbol();
}
