package org.fancyfsm.core.api.turing.tapes.iterators;

import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;

@RequiredArgsConstructor
public class ArrayTapeSpliterator<A> implements Spliterator<A> {

  private final A[] tape;
  private int index = 0;
  private long nonNullElementsCountCache = -1L;
  private long advancedItems = 0L;

  @Override
  public boolean tryAdvance(Consumer<? super A> action) {
    adjustIndex();
    if (index >= tape.length) return false;
    action.accept(tape[index++]);
    advancedItems++;
    return true;
  }

  @Override
  public Spliterator<A> trySplit() {
    adjustIndex();

    if (index >= tape.length) return Spliterators.emptySpliterator();

    int mid = (tape.length + index) / 2;

    var subSpliterator = new ArrayTapeSpliterator<>(Arrays.copyOfRange(tape, index, mid));

    advancedItems += subSpliterator.estimateSize();
    index = mid;

    return subSpliterator;
  }

  @Override
  public long estimateSize() {
    if (nonNullElementsCountCache == -1)
      nonNullElementsCountCache = determineNonNullElementsCount(tape.length);

    return nonNullElementsCountCache - advancedItems;
  }

  private long determineNonNullElementsCount(int endIndex) {
    long nonNullElementsCount = 0L;
    for (int i = 0; i < endIndex; i++) if (tape[i] != null) nonNullElementsCount++;
    return nonNullElementsCount;
  }

  private void adjustIndex() {
    while (index < tape.length && tape[index] == null) index++;
  }

  @Override
  public int characteristics() {
    return NONNULL | SIZED | SUBSIZED;
  }
}
