package org.fancyfsm.core.api.transitions.turing;

public interface HeadCommand<D> {
  D direction();
}
