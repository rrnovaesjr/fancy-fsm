package org.fancyfsm.core.api.deterministic.states;

import org.fancyfsm.core.api.deterministic.states.memory.DeterministicStateMemory;

public class DefaultDeterministicStateContext<S> extends DeterministicStateContext<S> {
  public DefaultDeterministicStateContext(S initialState, DeterministicStateMemory<S> stateMemory) {
    super(initialState, stateMemory);
  }
}
