package org.fancyfsm.core.api.turing.tapes.iterators;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;

public class BiDirectionalArrayTapeIterator<A> implements Iterator<A> {

  private final A[] negativeTape;
  private final A[] positiveTape;
  private int index;

  public BiDirectionalArrayTapeIterator(A[] negativeTape, A[] positiveTape) {
    this.negativeTape = Objects.requireNonNull(negativeTape, "Negative tape cannot be null");
    this.positiveTape = Objects.requireNonNull(positiveTape, "Positive tape cannot be null");
    index = -negativeTape.length;
  }

  @Override
  public boolean hasNext() {
    if (index >= positiveTape.length) return false;

    while (index < positiveTape.length && getSymbolOnCurrentIndex(index) == null) index++;

    return index < positiveTape.length;
  }

  @Override
  public A next() {
    if (!hasNext()) throw new NoSuchElementException("The iterator has no more elements");

    return getSymbolOnCurrentIndex(index++);
  }

  private A getSymbolOnCurrentIndex(int index) {
    if (index < 0) return negativeTape[-index - 1];
    return positiveTape[index];
  }
}
