package org.fancyfsm.core.api.turing.tapes.iterators;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.Iterator;
import java.util.NoSuchElementException;

@RequiredArgsConstructor
public class ArrayTapeIterator<A> implements Iterator<A> {

  @NonNull private final A[] tape;
  private int index = 0;

  @Override
  public boolean hasNext() {
    while (index < tape.length && tape[index] == null) index++;
    return index < tape.length;
  }

  @Override
  public A next() {
    if (!hasNext()) throw new NoSuchElementException("The iterator has no more elements");
    return tape[index++];
  }
}
