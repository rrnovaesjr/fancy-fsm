package org.fancyfsm.core.api.turing.tapes.functions.translators;

import lombok.NonNull;

import java.util.stream.Collector;
import java.util.stream.Stream;

public class CollectableAlphabetTranslator<A, C> extends AbstractIterableTapeTranslator<A, C> {

  @NonNull private final Collector<A, ?, C> collector;

  protected CollectableAlphabetTranslator(Class<A> symbolClass, Collector<A, ?, C> collector) {
    super(symbolClass);
    this.collector = collector;
  }

  @Override
  protected C doTranslate(Stream<A> tapeStream) {
    return tapeStream.collect(collector);
  }
}
