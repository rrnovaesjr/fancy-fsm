package org.fancyfsm.core.api.deterministic.states.memory;

import java.util.Optional;

public interface DeterministicStateMemory<S> {

  Optional<S> read();

  void write(S state);
}
