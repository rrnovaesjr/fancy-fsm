package org.fancyfsm.core.api.deterministic.states;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;
import org.fancyfsm.core.api.deterministic.states.memory.DeterministicStateMemory;

@Accessors(fluent = true)
@Getter
@RequiredArgsConstructor
public abstract class DeterministicStateContext<S> {

  private final S initialState;
  @NonNull private final DeterministicStateMemory<S> stateMemory;

  public S read() {
    return stateMemory.read().orElse(initialState);
  }

  public void write(S state) {
    stateMemory.write(state);
  }
}
